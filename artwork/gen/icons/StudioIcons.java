package icons;

import com.intellij.ui.IconManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * NOTE THIS FILE IS AUTO-GENERATED
 * DO NOT EDIT IT BY HAND, run "Generate icon classes" configuration instead
 */
public final class StudioIcons {
  private static @NotNull Icon load(@NotNull String path, int cacheKey, int flags) {
    return IconManager.getInstance().loadRasterizedIcon(path, StudioIcons.class.getClassLoader(), cacheKey, flags);
  }

  public static final class AppQualityInsights {
    /** 16x16 */ public static final @NotNull Icon ANR_INLINE = load("studio/icons/app-quality-insights/anr-inline.svg", 1273482726, 2);
    /** 16x16 */ public static final @NotNull Icon ANR_WITH_NOTE = load("studio/icons/app-quality-insights/anr-with-note.svg", 92672174, 2);
    /** 16x16 */ public static final @NotNull Icon ANR = load("studio/icons/app-quality-insights/anr.svg", -183291778, 2);
    /** 16x16 */ public static final @NotNull Icon DETAILS = load("studio/icons/app-quality-insights/details.svg", -405947826, 2);
    /** 16x16 */ public static final @NotNull Icon EARLY_SIGNAL = load("studio/icons/app-quality-insights/early-signal.svg", -691419033, 2);
    /** 16x16 */ public static final @NotNull Icon FATAL_INLINE = load("studio/icons/app-quality-insights/fatal-inline.svg", 920094852, 2);
    /** 16x16 */ public static final @NotNull Icon FATAL_WITH_NOTE = load("studio/icons/app-quality-insights/fatal-with-note.svg", -2019600921, 2);
    /** 16x16 */ public static final @NotNull Icon FATAL = load("studio/icons/app-quality-insights/fatal.svg", 141278751, 2);
    /** 16x16 */ public static final @NotNull Icon FRESH_SIGNAL = load("studio/icons/app-quality-insights/fresh-signal.svg", 1581652536, 2);
    /** 16x16 */ public static final @NotNull Icon ISSUE = load("studio/icons/app-quality-insights/issue.svg", -1684570446, 2);
    /** 16x16 */ public static final @NotNull Icon NON_FATAL_INLINE = load("studio/icons/app-quality-insights/non-fatal-inline.svg", 461248627, 2);
    /** 16x16 */ public static final @NotNull Icon NON_FATAL_WITH_NOTE = load("studio/icons/app-quality-insights/non-fatal-with-note.svg", 1287830171, 2);
    /** 16x16 */ public static final @NotNull Icon NON_FATAL = load("studio/icons/app-quality-insights/non-fatal.svg", 1142864120, 2);
    /** 16x16 */ public static final @NotNull Icon NOTE = load("studio/icons/app-quality-insights/note.svg", 1624234530, 2);
    /** 16x16 */ public static final @NotNull Icon REGRESSED_SIGNAL = load("studio/icons/app-quality-insights/regressed-signal.svg", 1017368808, 2);
    /** 16x16 */ public static final @NotNull Icon REPETITIVE_SIGNAL = load("studio/icons/app-quality-insights/repetitive-signal.svg", 2044476830, 2);
    /** 16x16 */ public static final @NotNull Icon SEND = load("studio/icons/app-quality-insights/send.svg", 1183985934, 2);
  }

  public static final class Assistant {
    public static final class AppLinks {
      /** 50x50 */ public static final @NotNull Icon STEP_1_LARGE = load("studio/icons/assistant/app-links/step-1-large.svg", 482592095, 2);
      /** 50x50 */ public static final @NotNull Icon STEP_2_LARGE = load("studio/icons/assistant/app-links/step-2-large.svg", -616037068, 2);
      /** 50x50 */ public static final @NotNull Icon STEP_3_LARGE = load("studio/icons/assistant/app-links/step-3-large.svg", -1310495728, 2);
      /** 50x50 */ public static final @NotNull Icon STEP_4_LARGE = load("studio/icons/assistant/app-links/step-4-large.svg", 84763394, 2);
    }
  }

  public static final class Avd {
    /** 16x16 */ public static final @NotNull Icon CONNECTION_GENERIC = load("studio/icons/avd/connection-generic.svg", 1123204568, 2);
    /** 16x16 */ public static final @NotNull Icon CONNECTION_USB = load("studio/icons/avd/connection-usb.svg", 1024452991, 2);
    /** 16x16 */ public static final @NotNull Icon CONNECTION_WIFI = load("studio/icons/avd/connection-wifi.svg", -492565049, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_AUTOMOTIVE = load("studio/icons/avd/device-automotive.svg", -1935848685, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_CAR = load("studio/icons/avd/device-car.svg", -97102198, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_DESKTOP = load("studio/icons/avd/device-desktop.svg", -455048627, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_GLASS = load("studio/icons/avd/device-glass.svg", 1132811788, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_MOBILE = load("studio/icons/avd/device-mobile.svg", 1371374684, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_PHONE = load("studio/icons/avd/device-phone.svg", 1986294525, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_PLAY_STORE = load("studio/icons/avd/device-play-store.svg", -1115902025, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_TABLET = load("studio/icons/avd/device-tablet.svg", 300435746, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_TV = load("studio/icons/avd/device-tv.svg", -1782349090, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_WEAR = load("studio/icons/avd/device-wear.svg", 1895840226, 2);
    /** 16x16 */ public static final @NotNull Icon EDIT = load("studio/icons/avd/edit.svg", 536223351, 2);
    /** 48x48 */ public static final @NotNull Icon LANDSCAPE = load("studio/icons/avd/landscape.svg", -440250516, 2);
    /** 16x16 */ public static final @NotNull Icon PAIR_OVER_WIFI = load("studio/icons/avd/pair-over-wifi.svg", 1460035612, 2);
    /** 48x48 */ public static final @NotNull Icon PORTRAIT = load("studio/icons/avd/portrait.svg", -466786301, 2);
    /** 16x16 */ public static final @NotNull Icon RUN = load("studio/icons/avd/run.svg", -442773063, 2);
    /** 16x16 */ public static final @NotNull Icon START_MIRROR = load("studio/icons/avd/start-mirror.svg", -1354985324, 2);
    /** 16x16 */ public static final @NotNull Icon START_RESERVATION = load("studio/icons/avd/start-reservation.svg", 1251812592, 2);
    /** 16x16 */ public static final @NotNull Icon STATUS_AVAILABLE_LOW = load("studio/icons/avd/status-available-low.svg", 175719711, 2);
    /** 16x16 */ public static final @NotNull Icon STATUS_AVAILABLE_MODERATE = load("studio/icons/avd/status-available-moderate.svg", 1995731218, 2);
    /** 16x16 */ public static final @NotNull Icon STATUS_DECORATOR_OFFLINE = load("studio/icons/avd/status-decorator-offline.svg", 1608725786, 2);
    /** 16x16 */ public static final @NotNull Icon STATUS_DECORATOR_ONLINE = load("studio/icons/avd/status-decorator-online.svg", -1183461252, 2);
    /** 16x16 */ public static final @NotNull Icon STOP_MIRROR = load("studio/icons/avd/stop-mirror.svg", 1143220939, 2);
    /** 16x16 */ public static final @NotNull Icon STOP = load("studio/icons/avd/stop.svg", 1854161946, 2);
  }

  public static final class Common {
    /** 16x16 */ public static final @NotNull Icon ADD = load("studio/icons/common/add.svg", 602302143, 2);
    /** 16x16 */ public static final @NotNull Icon ANDROID_HEAD = load("studio/icons/common/android-head.svg", 1430701932, 2);
    /** 16x16 */ public static final @NotNull Icon BACK_ARROW = load("studio/icons/common/back-arrow.svg", 1353723521, 2);
    /** 16x16 */ public static final @NotNull Icon CHECKED = load("studio/icons/common/checked.svg", 686818495, 2);
    /** 16x16 */ public static final @NotNull Icon CLEAR = load("studio/icons/common/clear.svg", -1668104303, 2);
    /** 16x16 */ public static final @NotNull Icon CLOSE = load("studio/icons/common/close.svg", 1963420904, 2);
    /** 16x16 */ public static final @NotNull Icon CONFIGURE_COLUMNS = load("studio/icons/common/configure-columns.svg", 385577014, 2);
    /** 16x16 */ public static final @NotNull Icon CROP = load("studio/icons/common/crop.svg", 1718407285, 2);
    /** 16x16 */ public static final @NotNull Icon DELETE = load("studio/icons/common/delete.svg", -1808522133, 2);
    /** 16x16 */ public static final @NotNull Icon DISLIKE = load("studio/icons/common/dislike.svg", 1222540936, 2);
    /** 16x16 */ public static final @NotNull Icon EDIT = load("studio/icons/common/edit.svg", 1240953326, 2);
    /** 16x16 */ public static final @NotNull Icon ERROR_DECORATOR = load("studio/icons/common/error-decorator.svg", 1896202002, 2);
    /** 16x16 */ public static final @NotNull Icon ERROR_INLINE = load("studio/icons/common/error-inline.svg", 1720039205, 2);
    /** 16x16 */ public static final @NotNull Icon ERROR_STACK = load("studio/icons/common/error-stack.svg", 688948122, 2);
    /** 16x16 */ public static final @NotNull Icon ERROR = load("studio/icons/common/error.svg", -681502686, 2);
    /** 16x16 */ public static final @NotNull Icon EXPORT = load("studio/icons/common/export.svg", -1643246964, 2);
    /** 16x16 */ public static final @NotNull Icon FILTER = load("studio/icons/common/filter.svg", 859227049, 2);
    /** 16x16 */ public static final @NotNull Icon GRID_VIEW = load("studio/icons/common/grid-view.svg", 574719072, 2);
    /** 16x16 */ public static final @NotNull Icon GROUP = load("studio/icons/common/group.svg", -891580263, 2);
    /** 16x16 */ public static final @NotNull Icon HELP = load("studio/icons/common/help.svg", 1735457681, 2);
    /** 16x16 */ public static final @NotNull Icon IMPORT_DATA = load("studio/icons/common/import-data.svg", -1776330266, 2);
    /** 16x16 */ public static final @NotNull Icon INFO_INLINE = load("studio/icons/common/info-inline.svg", 571727733, 2);
    /** 16x16 */ public static final @NotNull Icon INFO = load("studio/icons/common/info.svg", 1556195102, 2);
    /** 16x16 */ public static final @NotNull Icon LIKE = load("studio/icons/common/like.svg", -891796787, 2);
    /** 16x16 */ public static final @NotNull Icon LINK = load("studio/icons/common/link.svg", 550471904, 2);
    /** 16x16 */ public static final @NotNull Icon LIST_VIEW = load("studio/icons/common/list-view.svg", -618548541, 2);
    /** 16x16 */ public static final @NotNull Icon LOCKED_INLINE = load("studio/icons/common/locked-inline.svg", 1007831188, 2);
    /** 16x16 */ public static final @NotNull Icon LOGIN = load("studio/icons/common/login.svg", -1232394152, 2);
    /** 16x16 */ public static final @NotNull Icon LOGOUT = load("studio/icons/common/logout.svg", 2076736871, 2);
    /** 16x16 */ public static final @NotNull Icon MISSING = load("studio/icons/common/missing.svg", 553355273, 2);
    /** 16x16 */ public static final @NotNull Icon NAVIGATE_TO_SOURCE = load("studio/icons/common/navigate-to-source.svg", 1170184561, 2);
    /** 16x16 */ public static final @NotNull Icon OVERFLOW = load("studio/icons/common/overflow.svg", -1258376657, 2);
    /** 14x24 */ public static final @NotNull Icon PROPERTY_BOUND_FOCUS_LARGE = load("studio/icons/common/property-bound-focus-large.svg", 1820718930, 2);
    /** 14x20 */ public static final @NotNull Icon PROPERTY_BOUND_FOCUS = load("studio/icons/common/property-bound-focus.svg", 1005161610, 2);
    /** 14x24 */ public static final @NotNull Icon PROPERTY_BOUND_LARGE = load("studio/icons/common/property-bound-large.svg", 1396655306, 2);
    /** 14x20 */ public static final @NotNull Icon PROPERTY_BOUND = load("studio/icons/common/property-bound.svg", -626504476, 2);
    /** 14x24 */ public static final @NotNull Icon PROPERTY_UNBOUND_FOCUS_LARGE = load("studio/icons/common/property-unbound-focus-large.svg", 2040089452, 2);
    /** 14x20 */ public static final @NotNull Icon PROPERTY_UNBOUND_FOCUS = load("studio/icons/common/property-unbound-focus.svg", 236385299, 2);
    /** 14x24 */ public static final @NotNull Icon PROPERTY_UNBOUND_LARGE = load("studio/icons/common/property-unbound-large.svg", 1376544020, 2);
    /** 14x20 */ public static final @NotNull Icon PROPERTY_UNBOUND = load("studio/icons/common/property-unbound.svg", 615126358, 2);
    /** 16x16 */ public static final @NotNull Icon REMOVE = load("studio/icons/common/remove.svg", -832214104, 2);
    /** 16x16 */ public static final @NotNull Icon REORDER = load("studio/icons/common/reorder.svg", 1238545097, 2);
    /** 16x16 */ public static final @NotNull Icon RESET_ZOOM = load("studio/icons/common/reset-zoom.svg", -97115568, 2);
    /** 16x16 */ public static final @NotNull Icon SCREENSHOT = load("studio/icons/common/screenshot.svg", 264515500, 2);
    /** 16x16 */ public static final @NotNull Icon SEARCH = load("studio/icons/common/search.svg", -176527692, 2);
    /** 16x16 */ public static final @NotNull Icon SETTINGS = load("studio/icons/common/settings.svg", -490431849, 2);
    /** 16x16 */ public static final @NotNull Icon SUCCESS_INLINE = load("studio/icons/common/success-inline.svg", 567460173, 2);
    /** 16x16 */ public static final @NotNull Icon SUCCESS = load("studio/icons/common/success.svg", 259060302, 2);
    /** 16x16 */ public static final @NotNull Icon TYPO_STACK = load("studio/icons/common/typo-stack.svg", 1431329464, 2);
    /** 16x16 */ public static final @NotNull Icon VIDEO_CAPTURE = load("studio/icons/common/video-capture.svg", 2052100788, 2);
    /** 16x16 */ public static final @NotNull Icon VISIBILITY_INLINE = load("studio/icons/common/visibility-inline.svg", -1852305489, 2);
    /** 16x16 */ public static final @NotNull Icon WARNING_INLINE = load("studio/icons/common/warning-inline.svg", -1530976660, 2);
    /** 16x16 */ public static final @NotNull Icon WARNING_STACK = load("studio/icons/common/warning-stack.svg", 1052621161, 2);
    /** 16x16 */ public static final @NotNull Icon WARNING = load("studio/icons/common/warning.svg", -186390397, 2);
    /** 16x16 */ public static final @NotNull Icon WEAK_WARNING_STACK = load("studio/icons/common/weak-warning-stack.svg", -2103408848, 2);
    /** 16x16 */ public static final @NotNull Icon ZOOM_ACTUAL = load("studio/icons/common/zoom-actual.svg", 1982135018, 2);
    /** 16x16 */ public static final @NotNull Icon ZOOM_IN = load("studio/icons/common/zoom-in.svg", -2011505637, 2);
    /** 16x16 */ public static final @NotNull Icon ZOOM_OUT = load("studio/icons/common/zoom-out.svg", 651771443, 2);
    /** 16x16 */ public static final @NotNull Icon ZOOM_SELECT = load("studio/icons/common/zoom-select.svg", 1344939097, 2);
  }

  public static final class Compose {
    public static final class Editor {
      /** 16x16 */ public static final @NotNull Icon COMPOSABLE_FUNCTION = load("studio/icons/compose/editor/composable-function.svg", 887304315, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon ANIMATION_INSPECTOR = load("studio/icons/compose/toolbar/animation-inspector.svg", -1076008935, 2);
      /** 16x16 */ public static final @NotNull Icon FREEZE_ANIMATION = load("studio/icons/compose/toolbar/freeze-animation.svg", 1158658591, 2);
      /** 16x16 */ public static final @NotNull Icon INSPECT_PREVIEW = load("studio/icons/compose/toolbar/inspect-preview.svg", -1350042667, 2);
      /** 16x16 */ public static final @NotNull Icon INTERACTIVE_PREVIEW = load("studio/icons/compose/toolbar/interactive-preview.svg", 2092607021, 2);
      /** 16x16 */ public static final @NotNull Icon RUN_CONFIGURATION = load("studio/icons/compose/toolbar/run-configuration.svg", 837558325, 2);
      /** 16x16 */ public static final @NotNull Icon RUN_ON_DEVICE = load("studio/icons/compose/toolbar/run-on-device.svg", -892831922, 2);
      /** 16x16 */ public static final @NotNull Icon STOP_INTERACTIVE_MODE = load("studio/icons/compose/toolbar/stop-interactive-mode.svg", -2060841640, 2);
      /** 16x16 */ public static final @NotNull Icon UI_CHECK = load("studio/icons/compose/toolbar/ui-check.svg", -1453238880, 2);
    }
  }

  public static final class Cursors {
    /** 32x32 */ public static final @NotNull Icon EW_RESIZE = load("studio/icons/cursors/ew-resize.svg", 1232168473, 0);
    /** 32x32 */ public static final @NotNull Icon GRAB = load("studio/icons/cursors/grab.svg", -872784316, 0);
    /** 32x32 */ public static final @NotNull Icon GRABBING = load("studio/icons/cursors/grabbing.svg", 885134097, 0);
    /** 32x32 */ public static final @NotNull Icon MOVE = load("studio/icons/cursors/move.svg", -1371213609, 0);
    /** 32x32 */ public static final @NotNull Icon NESW_RESIZE = load("studio/icons/cursors/nesw-resize.svg", 2088697372, 0);
    /** 32x32 */ public static final @NotNull Icon NS_RESIZE = load("studio/icons/cursors/ns-resize.svg", 1338621294, 0);
    /** 32x32 */ public static final @NotNull Icon NWSE_RESIZE = load("studio/icons/cursors/nwse-resize.svg", 176207121, 0);
  }

  public static final class DatabaseInspector {
    /** 16x16 */ public static final @NotNull Icon ALLOW_DATABASES_TO_CLOSE = load("studio/icons/database-inspector/allow-databases-to-close.svg", 900538053, 2);
    /** 16x16 */ public static final @NotNull Icon COLUMN = load("studio/icons/database-inspector/column.svg", 963013752, 2);
    /** 16x16 */ public static final @NotNull Icon DATABASE_OFFLINE = load("studio/icons/database-inspector/database-offline.svg", 162215938, 2);
    /** 16x16 */ public static final @NotNull Icon DATABASE_UNAVAILABLE = load("studio/icons/database-inspector/database-unavailable.svg", -20404719, 2);
    /** 16x16 */ public static final @NotNull Icon DATABASE = load("studio/icons/database-inspector/database.svg", -482036357, 2);
    /** 16x16 */ public static final @NotNull Icon KEEP_DATABASES_OPEN = load("studio/icons/database-inspector/keep-databases-open.svg", 1677940140, 2);
    /** 16x16 */ public static final @NotNull Icon NEW_QUERY = load("studio/icons/database-inspector/new-query.svg", -373310205, 2);
    /** 16x16 */ public static final @NotNull Icon PRIMARY_KEY = load("studio/icons/database-inspector/primary-key.svg", 88944545, 2);
    /** 16x16 */ public static final @NotNull Icon SCHEMA = load("studio/icons/database-inspector/schema.svg", -526142837, 2);
    /** 16x16 */ public static final @NotNull Icon TABLE = load("studio/icons/database-inspector/table.svg", 1755661259, 2);
    /** 16x16 */ public static final @NotNull Icon VIEW = load("studio/icons/database-inspector/view.svg", 738072837, 2);
  }

  public static final class DeviceConfiguration {
    /** 16x16 */ public static final @NotNull Icon COUNTRY_CODE = load("studio/icons/device-configuration/country-code.svg", -938198021, 2);
    /** 16x16 */ public static final @NotNull Icon DENSITY = load("studio/icons/device-configuration/density.svg", -849655421, 2);
    /** 16x16 */ public static final @NotNull Icon DIMENSION = load("studio/icons/device-configuration/dimension.svg", -1862978142, 2);
    /** 16x16 */ public static final @NotNull Icon KEYBOARD = load("studio/icons/device-configuration/keyboard.svg", -1097783202, 2);
    /** 16x16 */ public static final @NotNull Icon LAYOUT_DIRECTION = load("studio/icons/device-configuration/layout-direction.svg", 680300923, 2);
    /** 16x16 */ public static final @NotNull Icon LOCALE = load("studio/icons/device-configuration/locale.svg", 557578447, 2);
    /** 16x16 */ public static final @NotNull Icon NAVIGATION_METHOD = load("studio/icons/device-configuration/navigation-method.svg", -69698381, 2);
    /** 16x16 */ public static final @NotNull Icon NAVIGATION_STATE = load("studio/icons/device-configuration/navigation-state.svg", -830885329, 2);
    /** 16x16 */ public static final @NotNull Icon NETWORK_CODE = load("studio/icons/device-configuration/network-code.svg", -739759782, 2);
    /** 16x16 */ public static final @NotNull Icon NIGHT_MODE = load("studio/icons/device-configuration/night-mode.svg", -759288288, 2);
    /** 16x16 */ public static final @NotNull Icon ORIENTATION = load("studio/icons/device-configuration/orientation.svg", 8709236, 2);
    /** 16x16 */ public static final @NotNull Icon SCREEN_HEIGHT = load("studio/icons/device-configuration/screen-height.svg", 863065354, 2);
    /** 16x16 */ public static final @NotNull Icon SCREEN_RATIO = load("studio/icons/device-configuration/screen-ratio.svg", 1036545672, 2);
    /** 16x16 */ public static final @NotNull Icon SCREEN_ROUNDNESS = load("studio/icons/device-configuration/screen-roundness.svg", -1881551423, 2);
    /** 16x16 */ public static final @NotNull Icon SCREEN_SIZE = load("studio/icons/device-configuration/screen-size.svg", 421198026, 2);
    /** 16x16 */ public static final @NotNull Icon SCREEN_WIDTH = load("studio/icons/device-configuration/screen-width.svg", 493258125, 2);
    /** 16x16 */ public static final @NotNull Icon SMALLEST_SCREEN_SIZE = load("studio/icons/device-configuration/smallest-screen-size.svg", 491778949, 2);
    /** 16x16 */ public static final @NotNull Icon TEXT_INPUT = load("studio/icons/device-configuration/text-input.svg", -1691219578, 2);
    /** 16x16 */ public static final @NotNull Icon TOUCH_SCREEN = load("studio/icons/device-configuration/touch-screen.svg", -1632323049, 2);
    /** 16x16 */ public static final @NotNull Icon UI_MODE = load("studio/icons/device-configuration/ui-mode.svg", -127192175, 2);
    /** 16x16 */ public static final @NotNull Icon VERSION = load("studio/icons/device-configuration/version.svg", -959895536, 2);
  }

  public static final class DeviceExplorer {
    /** 16x16 */ public static final @NotNull Icon DATABASE_FOLDER = load("studio/icons/device-explorer/database-folder.svg", -93335684, 2);
    /** 16x16 */ public static final @NotNull Icon DEVICE_PAIRED = load("studio/icons/device-explorer/device-paired.svg", -1498059348, 2);
    /** 16x16 */ public static final @NotNull Icon FIREBASE_DEVICE_CAR = load("studio/icons/device-explorer/firebase-device-car.svg", -1860525439, 2);
    /** 16x16 */ public static final @NotNull Icon FIREBASE_DEVICE_DESKTOP = load("studio/icons/device-explorer/firebase-device-desktop.svg", -1467704696, 2);
    /** 16x16 */ public static final @NotNull Icon FIREBASE_DEVICE_PHONE = load("studio/icons/device-explorer/firebase-device-phone.svg", -1898508598, 2);
    /** 16x16 */ public static final @NotNull Icon FIREBASE_DEVICE_TV = load("studio/icons/device-explorer/firebase-device-tv.svg", -1106992752, 2);
    /** 16x16 */ public static final @NotNull Icon FIREBASE_DEVICE_WEAR = load("studio/icons/device-explorer/firebase-device-wear.svg", -646782973, 2);
    /** 16x16 */ public static final @NotNull Icon MULTIPLE_DEVICES = load("studio/icons/device-explorer/multiple-devices.svg", -1092819663, 2);
    /** 16x16 */ public static final @NotNull Icon OEM_LAB_DEVICE_CAR = load("studio/icons/device-explorer/oem-lab-device-car.svg", 1416066005, 2);
    /** 16x16 */ public static final @NotNull Icon OEM_LAB_DEVICE_DESKTOP = load("studio/icons/device-explorer/oem-lab-device-desktop.svg", -524922808, 2);
    /** 16x16 */ public static final @NotNull Icon OEM_LAB_DEVICE_PHONE = load("studio/icons/device-explorer/oem-lab-device-phone.svg", 243467045, 2);
    /** 16x16 */ public static final @NotNull Icon OEM_LAB_DEVICE_TV = load("studio/icons/device-explorer/oem-lab-device-tv.svg", -1446845516, 2);
    /** 16x16 */ public static final @NotNull Icon OEM_LAB_DEVICE_WEAR = load("studio/icons/device-explorer/oem-lab-device-wear.svg", 1546987870, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_CAR = load("studio/icons/device-explorer/physical-device-car.svg", -2122060561, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_HEADSET = load("studio/icons/device-explorer/physical-device-headset.svg", -1057877647, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_PHONE = load("studio/icons/device-explorer/physical-device-phone.svg", -963364788, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_THINGS = load("studio/icons/device-explorer/physical-device-things.svg", 727266161, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_TV = load("studio/icons/device-explorer/physical-device-tv.svg", 1119472280, 2);
    /** 16x16 */ public static final @NotNull Icon PHYSICAL_DEVICE_WEAR = load("studio/icons/device-explorer/physical-device-wear.svg", 1373544433, 2);
    /** 16x16 */ public static final @NotNull Icon SD_CARD_FOLDER = load("studio/icons/device-explorer/sd-card-folder.svg", -1902804771, 2);
    /** 16x16 */ public static final @NotNull Icon SHARED_PREFS = load("studio/icons/device-explorer/shared-prefs.svg", -1084441711, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_CAR = load("studio/icons/device-explorer/virtual-device-car.svg", 1163863845, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_DESKTOP = load("studio/icons/device-explorer/virtual-device-desktop.svg", -2100863313, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_HEADSET = load("studio/icons/device-explorer/virtual-device-headset.svg", 1369285026, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_PHONE = load("studio/icons/device-explorer/virtual-device-phone.svg", 1453798273, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_TV = load("studio/icons/device-explorer/virtual-device-tv.svg", 1880403456, 2);
    /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICE_WEAR = load("studio/icons/device-explorer/virtual-device-wear.svg", 328663258, 2);
  }

  public static final class DeviceProcessMonitor {
    /** 16x16 */ public static final @NotNull Icon FORCE_STOP = load("studio/icons/device-process-monitor/force-stop.svg", -1962743465, 2);
    /** 16x16 */ public static final @NotNull Icon KILL_PROCESS = load("studio/icons/device-process-monitor/kill-process.svg", -1760976291, 2);
  }

  public static final class Emulator {
    public static final class Menu {
      /** 16x16 */ public static final @NotNull Icon MODE_DESKTOP = load("studio/icons/emulator/menu/mode-desktop.svg", 53666237, 2);
      /** 16x16 */ public static final @NotNull Icon MODE_FOLDABLE = load("studio/icons/emulator/menu/mode-foldable.svg", -221192790, 2);
      /** 16x16 */ public static final @NotNull Icon MODE_PHONE = load("studio/icons/emulator/menu/mode-phone.svg", -1535602568, 2);
      /** 16x16 */ public static final @NotNull Icon MODE_TABLET = load("studio/icons/emulator/menu/mode-tablet.svg", 1311162423, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_CLOSED = load("studio/icons/emulator/menu/posture-closed.svg", -208195842, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_DUAL_DISPLAY = load("studio/icons/emulator/menu/posture-dual-display.svg", -585666285, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_FLIPPED = load("studio/icons/emulator/menu/posture-flipped.svg", -1967064600, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_HALF_FOLDED = load("studio/icons/emulator/menu/posture-half-folded.svg", -1192071867, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_OPEN = load("studio/icons/emulator/menu/posture-open.svg", 1894268789, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_REAR_DISPLAY = load("studio/icons/emulator/menu/posture-rear-display.svg", 2127104859, 2);
      /** 16x16 */ public static final @NotNull Icon POSTURE_TENT = load("studio/icons/emulator/menu/posture-tent.svg", -957071240, 2);
    }

    public static final class Snapshots {
      /** 16x16 */ public static final @NotNull Icon INVALID_SNAPSHOT_DECORATOR = load("studio/icons/emulator/snapshots/invalid-snapshot-decorator.svg", 1267648239, 2);
      /** 16x16 */ public static final @NotNull Icon LOAD_SNAPSHOT = load("studio/icons/emulator/snapshots/load-snapshot.svg", -153368345, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon BACK = load("studio/icons/emulator/toolbar/back.svg", 95186328, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_SETTINGS = load("studio/icons/emulator/toolbar/device-settings.svg", -1030533288, 2);
      /** 16x16 */ public static final @NotNull Icon FOLD = load("studio/icons/emulator/toolbar/fold.svg", 795929091, 2);
      /** 16x16 */ public static final @NotNull Icon HARDWARE_INPUT = load("studio/icons/emulator/toolbar/hardware-input.svg", 1434283793, 2);
      /** 16x16 */ public static final @NotNull Icon HOME = load("studio/icons/emulator/toolbar/home.svg", -1928100497, 2);
      /** 16x16 */ public static final @NotNull Icon OVERVIEW = load("studio/icons/emulator/toolbar/overview.svg", -1694917545, 2);
      /** 16x16 */ public static final @NotNull Icon POWER_MENU = load("studio/icons/emulator/toolbar/power-menu.svg", -1778286724, 2);
      /** 16x16 */ public static final @NotNull Icon POWER = load("studio/icons/emulator/toolbar/power.svg", -680094051, 2);
      /** 16x16 */ public static final @NotNull Icon RESIZE_DESKTOP = load("studio/icons/emulator/toolbar/resize-desktop.svg", 1419714402, 2);
      /** 16x16 */ public static final @NotNull Icon RESIZE_FOLDABLE = load("studio/icons/emulator/toolbar/resize-foldable.svg", 1412827722, 2);
      /** 16x16 */ public static final @NotNull Icon RESIZE_PHONE = load("studio/icons/emulator/toolbar/resize-phone.svg", -782811555, 2);
      /** 16x16 */ public static final @NotNull Icon RESIZE_TABLET = load("studio/icons/emulator/toolbar/resize-tablet.svg", -624241202, 2);
      /** 16x16 */ public static final @NotNull Icon ROTATE_LEFT = load("studio/icons/emulator/toolbar/rotate-left.svg", 1669108261, 2);
      /** 16x16 */ public static final @NotNull Icon ROTATE_RIGHT = load("studio/icons/emulator/toolbar/rotate-right.svg", -1456522707, 2);
      /** 16x16 */ public static final @NotNull Icon SCREENSHOT = load("studio/icons/emulator/toolbar/screenshot.svg", 264515500, 2);
      /** 16x16 */ public static final @NotNull Icon SESSION = load("studio/icons/emulator/toolbar/session.svg", 1558540732, 2);
      /** 16x16 */ public static final @NotNull Icon SNAPSHOTS = load("studio/icons/emulator/toolbar/snapshots.svg", 714983074, 2);
      /** 16x16 */ public static final @NotNull Icon VOLUME_DOWN = load("studio/icons/emulator/toolbar/volume-down.svg", -1970522142, 2);
      /** 16x16 */ public static final @NotNull Icon VOLUME_UP = load("studio/icons/emulator/toolbar/volume-up.svg", -219880749, 2);
    }

    public static final class Wear {
      /** 16x16 */ public static final @NotNull Icon BUTTON_1 = load("studio/icons/emulator/wear/button-1.svg", 1286530699, 2);
      /** 16x16 */ public static final @NotNull Icon BUTTON_2 = load("studio/icons/emulator/wear/button-2.svg", -1436979428, 2);
      /** 16x16 */ public static final @NotNull Icon HEALTH_SERVICES = load("studio/icons/emulator/wear/health-services.svg", -1655456109, 2);
      /** 16x16 */ public static final @NotNull Icon PALM = load("studio/icons/emulator/wear/palm.svg", -793095631, 2);
      /** 16x16 */ public static final @NotNull Icon TILT = load("studio/icons/emulator/wear/tilt.svg", -1479178131, 2);
    }

    public static final class XR {
      /** 16x16 */ public static final @NotNull Icon DOLLY = load("studio/icons/emulator/xr/dolly.svg", 628842988, 2);
      /** 16x16 */ public static final @NotNull Icon ENVIRONMENT_DAY = load("studio/icons/emulator/xr/environment-day.svg", 944839085, 2);
      /** 16x16 */ public static final @NotNull Icon ENVIRONMENT_NIGHT = load("studio/icons/emulator/xr/environment-night.svg", 979005270, 2);
      /** 16x16 */ public static final @NotNull Icon ENVIRONMENT = load("studio/icons/emulator/xr/environment.svg", 1649264994, 2);
      /** 16x16 */ public static final @NotNull Icon EYE_GAZE = load("studio/icons/emulator/xr/eye-gaze.svg", 1143477700, 2);
      /** 16x16 */ public static final @NotNull Icon HAND_TRACKING = load("studio/icons/emulator/xr/hand-tracking.svg", -703037275, 2);
      /** 16x16 */ public static final @NotNull Icon MOUSE_KEYBOARD_MODE = load("studio/icons/emulator/xr/mouse-keyboard-mode.svg", 1086672251, 2);
      /** 16x16 */ public static final @NotNull Icon PAN = load("studio/icons/emulator/xr/pan.svg", 1508546105, 2);
      /** 16x16 */ public static final @NotNull Icon PASSTHROUGH_OFF = load("studio/icons/emulator/xr/passthrough-off.svg", -1174513329, 2);
      /** 16x16 */ public static final @NotNull Icon PASSTHROUGH_ON = load("studio/icons/emulator/xr/passthrough-on.svg", -88168562, 2);
      /** 16x16 */ public static final @NotNull Icon RESET_POSITION = load("studio/icons/emulator/xr/reset-position.svg", 317141982, 2);
      /** 16x16 */ public static final @NotNull Icon ROTATE = load("studio/icons/emulator/xr/rotate.svg", 2102946050, 2);
      /** 16x16 */ public static final @NotNull Icon TASKBAR = load("studio/icons/emulator/xr/taskbar.svg", 1483123195, 2);
    }
  }

  public static final class GutterIcons {
    /** 14x14 */ public static final @NotNull Icon APP_QUALITY_INSIGHTS = load("studio/icons/gutter-icons/app-quality-insights.svg", -265928051, 2);
    /** 14x14 */ public static final @NotNull Icon COMPOSABLE_FUNCTION = load("studio/icons/gutter-icons/composable-function.svg", -528231568, 2);
    /** 14x14 */ public static final @NotNull Icon DEPENDENCY_CONSUMER = load("studio/icons/gutter-icons/dependency-consumer.svg", 506393402, 2);
    /** 14x14 */ public static final @NotNull Icon DEPENDENCY_PROVIDER = load("studio/icons/gutter-icons/dependency-provider.svg", -616670527, 2);
    /** 14x14 */ public static final @NotNull Icon ISSUE = load("studio/icons/gutter-icons/issue.svg", -1892210775, 2);
    /** 14x14 */ public static final @NotNull Icon PREVIEW_SETTINGS = load("studio/icons/gutter-icons/preview-settings.svg", 800020290, 2);
    /** 14x14 */ public static final @NotNull Icon RESOURCE_PICKER = load("studio/icons/gutter-icons/resource-picker.svg", 938601496, 2);
    /** 14x14 */ public static final @NotNull Icon RUN_ON_DEVICE = load("studio/icons/gutter-icons/run-on-device.svg", -1024856152, 2);
  }

  public static final class LayoutEditor {
    public static final class Extras {
      /** 24x24 */ public static final @NotNull Icon DOWNLOAD_OVERLAY_LEGACY_LARGE = load("studio/icons/layout-editor/extras/download-overlay-legacy-large.svg", 1681769118, 2);
      /** 16x16 */ public static final @NotNull Icon DOWNLOAD_OVERLAY_LEGACY = load("studio/icons/layout-editor/extras/download-overlay-legacy.svg", -179322449, 2);
      /** 16x16 */ public static final @NotNull Icon PALETTE_DOWNLOAD = load("studio/icons/layout-editor/extras/palette-download.svg", 954056603, 2);
      /** 24x24 */ public static final @NotNull Icon PIPETTE_LARGE = load("studio/icons/layout-editor/extras/pipette-large.svg", -1164648323, 2);
      /** 16x16 */ public static final @NotNull Icon PIPETTE = load("studio/icons/layout-editor/extras/pipette.svg", 1872788187, 2);
      /** 16x16 */ public static final @NotNull Icon ROOT_INLINE = load("studio/icons/layout-editor/extras/root-inline.svg", 1697588776, 2);
      /** 16x16 */ public static final @NotNull Icon VISIBILITY_GONE_INLINE = load("studio/icons/layout-editor/extras/visibility-gone-inline.svg", -909513502, 2);
    }

    public static final class Menu {
      /** 16x16 */ public static final @NotNull Icon CAST = load("studio/icons/layout-editor/menu/cast.svg", -227028186, 2);
      /** 16x16 */ public static final @NotNull Icon GROUP = load("studio/icons/layout-editor/menu/group.svg", -891580263, 2);
      /** 16x16 */ public static final @NotNull Icon ITEM = load("studio/icons/layout-editor/menu/item.svg", -1884988588, 2);
      /** 16x16 */ public static final @NotNull Icon MENU = load("studio/icons/layout-editor/menu/menu.svg", 1103205178, 2);
      /** 16x16 */ public static final @NotNull Icon SEARCH = load("studio/icons/layout-editor/menu/search.svg", 773949478, 2);
      /** 16x16 */ public static final @NotNull Icon SWITCH = load("studio/icons/layout-editor/menu/switch.svg", 2023182013, 2);
    }

    public static final class Motion {
      /** 16x16 */ public static final @NotNull Icon ADD_CONSTRAINT_SET = load("studio/icons/layout-editor/motion/add-constraint-set.svg", -489526511, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_GESTURE = load("studio/icons/layout-editor/motion/add-gesture.svg", 79123031, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_KEYFRAME = load("studio/icons/layout-editor/motion/add-keyframe.svg", -295712872, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_TRANSITION = load("studio/icons/layout-editor/motion/add-transition.svg", 1530409748, 2);
      /** 16x16 */ public static final @NotNull Icon BASE_LAYOUT = load("studio/icons/layout-editor/motion/base-layout.svg", -1519314951, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAINT_SET = load("studio/icons/layout-editor/motion/constraint-set.svg", 109695104, 2);
      /** 16x16 */ public static final @NotNull Icon END_CONSTRAINT = load("studio/icons/layout-editor/motion/end-constraint.svg", -4388543, 2);
      /** 16x16 */ public static final @NotNull Icon GESTURE = load("studio/icons/layout-editor/motion/gesture.svg", -999329514, 2);
      /** 16x16 */ public static final @NotNull Icon GO_TO_END = load("studio/icons/layout-editor/motion/go-to-end.svg", 1700747669, 2);
      /** 16x16 */ public static final @NotNull Icon GO_TO_START = load("studio/icons/layout-editor/motion/go-to-start.svg", 1400773605, 2);
      /** 16x16 */ public static final @NotNull Icon KEYFRAME = load("studio/icons/layout-editor/motion/keyframe.svg", -972779207, 2);
      /** 16x16 */ public static final @NotNull Icon LOOP = load("studio/icons/layout-editor/motion/loop.svg", -1002003230, 2);
      /** 16x16 */ public static final @NotNull Icon MAX_SCALE = load("studio/icons/layout-editor/motion/max-scale.svg", -1264588990, 2);
      /** 16x16 */ public static final @NotNull Icon MIN_SCALE = load("studio/icons/layout-editor/motion/min-scale.svg", -1549124079, 2);
      /** 16x16 */ public static final @NotNull Icon MOTION_LAYOUT = load("studio/icons/layout-editor/motion/motion-layout.svg", 371487201, 2);
      /** 16x16 */ public static final @NotNull Icon NEXT_TICK = load("studio/icons/layout-editor/motion/next-tick.svg", -201264581, 2);
      /** 16x16 */ public static final @NotNull Icon PAUSE = load("studio/icons/layout-editor/motion/pause.svg", -1988049188, 2);
      /** 16x16 */ public static final @NotNull Icon PLAY_BACKWARD = load("studio/icons/layout-editor/motion/play-backward.svg", -1945558109, 2);
      /** 16x16 */ public static final @NotNull Icon PLAY_FORWARD = load("studio/icons/layout-editor/motion/play-forward.svg", -13465734, 2);
      /** 16x16 */ public static final @NotNull Icon PLAY_YOYO = load("studio/icons/layout-editor/motion/play-yoyo.svg", 1371268737, 2);
      /** 16x16 */ public static final @NotNull Icon PLAY = load("studio/icons/layout-editor/motion/play.svg", 1659925756, 2);
      /** 16x16 */ public static final @NotNull Icon PREVIOUS_TICK = load("studio/icons/layout-editor/motion/previous-tick.svg", 1624050281, 2);
      /** 16x16 */ public static final @NotNull Icon SLOW_MOTION = load("studio/icons/layout-editor/motion/slow-motion.svg", 319081406, 2);
      /** 16x16 */ public static final @NotNull Icon START_CONSTRAINT = load("studio/icons/layout-editor/motion/start-constraint.svg", 270727325, 2);
      /** 16x16 */ public static final @NotNull Icon TIMELINE_ADD = load("studio/icons/layout-editor/motion/timeline-add.svg", 1397856025, 2);
      /** 16x16 */ public static final @NotNull Icon TIMELINE_END_CONSTRAINT = load("studio/icons/layout-editor/motion/timeline-end-constraint.svg", 761484640, 2);
      /** 16x16 */ public static final @NotNull Icon TIMELINE_KEYFRAME_HEADER = load("studio/icons/layout-editor/motion/timeline-keyframe-header.svg", 395668796, 2);
      /** 16x16 */ public static final @NotNull Icon TIMELINE_KEYFRAME = load("studio/icons/layout-editor/motion/timeline-keyframe.svg", -1947178143, 2);
      /** 16x16 */ public static final @NotNull Icon TIMELINE_START_CONSTRAINT = load("studio/icons/layout-editor/motion/timeline-start-constraint.svg", 1645209585, 2);
      /** 16x16 */ public static final @NotNull Icon TRANSITION = load("studio/icons/layout-editor/motion/transition.svg", -118734703, 2);
    }

    public static final class Palette {
      /** 16x16 */ public static final @NotNull Icon AD_VIEW = load("studio/icons/layout-editor/palette/ad-view.svg", 1574882646, 2);
      /** 16x16 */ public static final @NotNull Icon ADAPTER_VIEW_FLIPPER = load("studio/icons/layout-editor/palette/adapter-view-flipper.svg", 1593779413, 2);
      /** 16x16 */ public static final @NotNull Icon ANALOG_CLOCK = load("studio/icons/layout-editor/palette/analog-clock.svg", -1852657773, 2);
      /** 16x16 */ public static final @NotNull Icon APP_BAR_LAYOUT = load("studio/icons/layout-editor/palette/app-bar-layout.svg", 739192181, 2);
      /** 16x16 */ public static final @NotNull Icon AUTO_COMPLETE_TEXT_VIEW = load("studio/icons/layout-editor/palette/auto-complete-text-view.svg", 2060434063, 2);
      /** 16x16 */ public static final @NotNull Icon BARRIER_HORIZONTAL = load("studio/icons/layout-editor/palette/barrier-horizontal.svg", -1157467827, 2);
      /** 16x16 */ public static final @NotNull Icon BARRIER_VERTICAL = load("studio/icons/layout-editor/palette/barrier-vertical.svg", -62349726, 2);
      /** 16x16 */ public static final @NotNull Icon BOTTOM_APP_BAR = load("studio/icons/layout-editor/palette/bottom-app-bar.svg", 686325008, 2);
      /** 16x16 */ public static final @NotNull Icon BOTTOM_NAVIGATION_VIEW = load("studio/icons/layout-editor/palette/bottom-navigation-view.svg", 841790757, 2);
      /** 16x16 */ public static final @NotNull Icon BUTTON = load("studio/icons/layout-editor/palette/button.svg", -2052589154, 2);
      /** 16x16 */ public static final @NotNull Icon CALENDAR_VIEW = load("studio/icons/layout-editor/palette/calendar-view.svg", 918004735, 2);
      /** 16x16 */ public static final @NotNull Icon CARD_VIEW = load("studio/icons/layout-editor/palette/card-view.svg", 811310222, 2);
      /** 16x16 */ public static final @NotNull Icon CHECK_BOX = load("studio/icons/layout-editor/palette/check-box.svg", 826895182, 2);
      /** 16x16 */ public static final @NotNull Icon CHECKED_TEXT_VIEW = load("studio/icons/layout-editor/palette/checked-text-view.svg", -1011167888, 2);
      /** 16x16 */ public static final @NotNull Icon CHIP_GROUP = load("studio/icons/layout-editor/palette/chip-group.svg", -1337611449, 2);
      /** 16x16 */ public static final @NotNull Icon CHIP = load("studio/icons/layout-editor/palette/chip.svg", 16015806, 2);
      /** 16x16 */ public static final @NotNull Icon CHRONOMETER = load("studio/icons/layout-editor/palette/chronometer.svg", 1382916871, 2);
      /** 16x16 */ public static final @NotNull Icon COLLAPSING_TOOLBAR_LAYOUT = load("studio/icons/layout-editor/palette/collapsing-toolbar-layout.svg", -36183646, 2);
      /** 16x16 */ public static final @NotNull Icon COMPOSABLE_FUNCTION = load("studio/icons/layout-editor/palette/composable-function.svg", -1769761747, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAINT_HELPER = load("studio/icons/layout-editor/palette/constraint-helper.svg", 841820293, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAINT_LAYOUT = load("studio/icons/layout-editor/palette/constraint-layout.svg", 1572695654, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAINT_SET = load("studio/icons/layout-editor/palette/constraint-set.svg", 109695104, 2);
      /** 16x16 */ public static final @NotNull Icon COORDINATOR_LAYOUT = load("studio/icons/layout-editor/palette/coordinator-layout.svg", -334615518, 2);
      /** 16x16 */ public static final @NotNull Icon CUSTOM_VIEW = load("studio/icons/layout-editor/palette/custom-view.svg", -1726943016, 2);
      /** 16x16 */ public static final @NotNull Icon DATE_PICKER = load("studio/icons/layout-editor/palette/date-picker.svg", 926501194, 2);
      /** 16x16 */ public static final @NotNull Icon DATE_TEXTFIELD = load("studio/icons/layout-editor/palette/date-textfield.svg", 1110303580, 2);
      /** 16x16 */ public static final @NotNull Icon DIALOG_POPUP = load("studio/icons/layout-editor/palette/dialog-popup.svg", 565812973, 2);
      /** 16x16 */ public static final @NotNull Icon EDIT_TEXT = load("studio/icons/layout-editor/palette/edit-text.svg", -1831812351, 2);
      /** 16x16 */ public static final @NotNull Icon EMAIL_TEXTFIELD = load("studio/icons/layout-editor/palette/email-textfield.svg", 527770612, 2);
      /** 16x16 */ public static final @NotNull Icon EXPANDABLE_LIST_VIEW = load("studio/icons/layout-editor/palette/expandable-list-view.svg", -1798027904, 2);
      /** 16x16 */ public static final @NotNull Icon FLOATING_ACTION_BUTTON = load("studio/icons/layout-editor/palette/floating-action-button.svg", 1583639259, 2);
      /** 16x16 */ public static final @NotNull Icon FLOW = load("studio/icons/layout-editor/palette/flow.svg", 261951631, 2);
      /** 16x16 */ public static final @NotNull Icon FRAGMENT = load("studio/icons/layout-editor/palette/fragment.svg", -1626562296, 2);
      /** 16x16 */ public static final @NotNull Icon FRAME_LAYOUT = load("studio/icons/layout-editor/palette/frame-layout.svg", -1914497050, 2);
      /** 16x16 */ public static final @NotNull Icon GRID_LAYOUT_COMPAT = load("studio/icons/layout-editor/palette/grid-layout-compat.svg", 1436582110, 2);
      /** 16x16 */ public static final @NotNull Icon GRID_LAYOUT = load("studio/icons/layout-editor/palette/grid-layout.svg", -1923601646, 2);
      /** 16x16 */ public static final @NotNull Icon GRID_VIEW = load("studio/icons/layout-editor/palette/grid-view.svg", -1754314201, 2);
      /** 16x16 */ public static final @NotNull Icon GROUP = load("studio/icons/layout-editor/palette/group.svg", 238248286, 2);
      /** 16x16 */ public static final @NotNull Icon GUIDELINE_HORIZONTAL = load("studio/icons/layout-editor/palette/guideline-horizontal.svg", 215940118, 2);
      /** 16x16 */ public static final @NotNull Icon GUIDELINE_VERTICAL = load("studio/icons/layout-editor/palette/guideline-vertical.svg", 1206123004, 2);
      /** 16x16 */ public static final @NotNull Icon HORIZONTAL_DIVIDER = load("studio/icons/layout-editor/palette/horizontal-divider.svg", -1803293829, 2);
      /** 16x16 */ public static final @NotNull Icon HORIZONTAL_SCROLL_VIEW = load("studio/icons/layout-editor/palette/horizontal-scroll-view.svg", 1709742488, 2);
      /** 16x16 */ public static final @NotNull Icon IMAGE_BUTTON = load("studio/icons/layout-editor/palette/image-button.svg", -1875919483, 2);
      /** 16x16 */ public static final @NotNull Icon IMAGE_SWITCHER = load("studio/icons/layout-editor/palette/image-switcher.svg", 1909936282, 2);
      /** 16x16 */ public static final @NotNull Icon IMAGE_VIEW = load("studio/icons/layout-editor/palette/image-view.svg", 1402154280, 2);
      /** 16x16 */ public static final @NotNull Icon INCLUDE = load("studio/icons/layout-editor/palette/include.svg", 1198464716, 2);
      /** 16x16 */ public static final @NotNull Icon LAYER = load("studio/icons/layout-editor/palette/layer.svg", -1826521368, 2);
      /** 16x16 */ public static final @NotNull Icon LINEAR_LAYOUT_HORZ = load("studio/icons/layout-editor/palette/linear-layout-horz.svg", -1270888907, 2);
      /** 16x16 */ public static final @NotNull Icon LINEAR_LAYOUT_VERT = load("studio/icons/layout-editor/palette/linear-layout-vert.svg", -561052554, 2);
      /** 16x16 */ public static final @NotNull Icon LIST_VIEW = load("studio/icons/layout-editor/palette/list-view.svg", 1284080706, 2);
      /** 16x16 */ public static final @NotNull Icon MAP_FRAGMENT = load("studio/icons/layout-editor/palette/map-fragment.svg", 1005939008, 2);
      /** 16x16 */ public static final @NotNull Icon MAP_VIEW = load("studio/icons/layout-editor/palette/map-view.svg", 23653964, 2);
      /** 16x16 */ public static final @NotNull Icon MENU = load("studio/icons/layout-editor/palette/menu.svg", -1602357807, 2);
      /** 16x16 */ public static final @NotNull Icon MERGE = load("studio/icons/layout-editor/palette/merge.svg", -62456922, 2);
      /** 16x16 */ public static final @NotNull Icon MULTI_AUTO_COMPLETE_TEXT_VIEW = load("studio/icons/layout-editor/palette/multi-auto-complete-text-view.svg", 1327369531, 2);
      /** 16x16 */ public static final @NotNull Icon NAV_HOST_FRAGMENT = load("studio/icons/layout-editor/palette/nav-host-fragment.svg", -247677922, 2);
      /** 16x16 */ public static final @NotNull Icon NAVIGATION_VIEW = load("studio/icons/layout-editor/palette/navigation-view.svg", -2025045924, 2);
      /** 16x16 */ public static final @NotNull Icon NESTED_SCROLL_VIEW = load("studio/icons/layout-editor/palette/nested-scroll-view.svg", -428615269, 2);
      /** 16x16 */ public static final @NotNull Icon NUMBER_DECIMAL_TEXTFIELD = load("studio/icons/layout-editor/palette/number-decimal-textfield.svg", 662877471, 2);
      /** 16x16 */ public static final @NotNull Icon NUMBER_PICKER = load("studio/icons/layout-editor/palette/number-picker.svg", -831059921, 2);
      /** 16x16 */ public static final @NotNull Icon NUMBER_SIGNED_TEXTFIELD = load("studio/icons/layout-editor/palette/number-signed-textfield.svg", 1049860778, 2);
      /** 16x16 */ public static final @NotNull Icon NUMBER_TEXTFIELD = load("studio/icons/layout-editor/palette/number-textfield.svg", -156390008, 2);
      /** 16x16 */ public static final @NotNull Icon PASSWORD_NUMERIC_TEXTFIELD = load("studio/icons/layout-editor/palette/password-numeric-textfield.svg", -1009927414, 2);
      /** 16x16 */ public static final @NotNull Icon PASSWORD_TEXTFIELD = load("studio/icons/layout-editor/palette/password-textfield.svg", -1421986847, 2);
      /** 16x16 */ public static final @NotNull Icon PHONE_TEXTFIELD = load("studio/icons/layout-editor/palette/phone-textfield.svg", -191376690, 2);
      /** 16x16 */ public static final @NotNull Icon PLACEHOLDER = load("studio/icons/layout-editor/palette/placeholder.svg", 501491327, 2);
      /** 16x16 */ public static final @NotNull Icon POSTAL_ADDRESS_TEXTFIELD = load("studio/icons/layout-editor/palette/postal-address-textfield.svg", 1707968290, 2);
      /** 16x16 */ public static final @NotNull Icon PROGRESS_BAR_HORIZONTAL = load("studio/icons/layout-editor/palette/progress-bar-horizontal.svg", -90303025, 2);
      /** 16x16 */ public static final @NotNull Icon PROGRESS_BAR = load("studio/icons/layout-editor/palette/progress-bar.svg", 244426300, 2);
      /** 16x16 */ public static final @NotNull Icon QUICK_CONTACT_BADGE = load("studio/icons/layout-editor/palette/quick-contact-badge.svg", 541207830, 2);
      /** 16x16 */ public static final @NotNull Icon RADIO_BUTTON = load("studio/icons/layout-editor/palette/radio-button.svg", 568759287, 2);
      /** 16x16 */ public static final @NotNull Icon RADIO_GROUP = load("studio/icons/layout-editor/palette/radio-group.svg", -705506268, 2);
      /** 16x16 */ public static final @NotNull Icon RATING_BAR = load("studio/icons/layout-editor/palette/rating-bar.svg", -441792074, 2);
      /** 16x16 */ public static final @NotNull Icon RECYCLER_VIEW = load("studio/icons/layout-editor/palette/recycler-view.svg", -1850312327, 2);
      /** 16x16 */ public static final @NotNull Icon RELATIVE_LAYOUT = load("studio/icons/layout-editor/palette/relative-layout.svg", 158044662, 2);
      /** 16x16 */ public static final @NotNull Icon REQUEST_FOCUS = load("studio/icons/layout-editor/palette/request-focus.svg", 1739066311, 2);
      /** 16x16 */ public static final @NotNull Icon SCROLL_VIEW = load("studio/icons/layout-editor/palette/scroll-view.svg", -1496783829, 2);
      /** 16x16 */ public static final @NotNull Icon SEARCH_VIEW = load("studio/icons/layout-editor/palette/search-view.svg", -1193299346, 2);
      /** 16x16 */ public static final @NotNull Icon SEEK_BAR_DISCRETE = load("studio/icons/layout-editor/palette/seek-bar-discrete.svg", -652406230, 2);
      /** 16x16 */ public static final @NotNull Icon SEEK_BAR = load("studio/icons/layout-editor/palette/seek-bar.svg", -2026158361, 2);
      /** 16x16 */ public static final @NotNull Icon SPACE = load("studio/icons/layout-editor/palette/space.svg", 1874193199, 2);
      /** 16x16 */ public static final @NotNull Icon SPINNER = load("studio/icons/layout-editor/palette/spinner.svg", -1152001504, 2);
      /** 16x16 */ public static final @NotNull Icon STACK_VIEW = load("studio/icons/layout-editor/palette/stack-view.svg", 1039316091, 2);
      /** 16x16 */ public static final @NotNull Icon SURFACE_VIEW = load("studio/icons/layout-editor/palette/surface-view.svg", 1896770027, 2);
      /** 16x16 */ public static final @NotNull Icon SWITCH = load("studio/icons/layout-editor/palette/switch.svg", 2023182013, 2);
      /** 16x16 */ public static final @NotNull Icon TAB_HOST = load("studio/icons/layout-editor/palette/tab-host.svg", 818952714, 2);
      /** 16x16 */ public static final @NotNull Icon TAB_ITEM = load("studio/icons/layout-editor/palette/tab-item.svg", 6525634, 2);
      /** 16x16 */ public static final @NotNull Icon TAB_LAYOUT = load("studio/icons/layout-editor/palette/tab-layout.svg", 2077654756, 2);
      /** 16x16 */ public static final @NotNull Icon TAB_WIDGET = load("studio/icons/layout-editor/palette/tab-widget.svg", 942940373, 2);
      /** 16x16 */ public static final @NotNull Icon TABLE_LAYOUT = load("studio/icons/layout-editor/palette/table-layout.svg", 2104567284, 2);
      /** 16x16 */ public static final @NotNull Icon TABLE_ROW = load("studio/icons/layout-editor/palette/table-row.svg", -2125854369, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_CLOCK = load("studio/icons/layout-editor/palette/text-clock.svg", 1046294342, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_INPUT_LAYOUT = load("studio/icons/layout-editor/palette/text-input-layout.svg", 1540557455, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_SWITCHER = load("studio/icons/layout-editor/palette/text-switcher.svg", 1178512073, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_VIEW = load("studio/icons/layout-editor/palette/text-view.svg", 540818713, 2);
      /** 16x16 */ public static final @NotNull Icon TEXTFIELD_MULTILINE = load("studio/icons/layout-editor/palette/textfield-multiline.svg", -656812594, 2);
      /** 16x16 */ public static final @NotNull Icon TEXTFIELD = load("studio/icons/layout-editor/palette/textfield.svg", -1192247509, 2);
      /** 16x16 */ public static final @NotNull Icon TEXTURE_VIEW = load("studio/icons/layout-editor/palette/texture-view.svg", 731617631, 2);
      /** 16x16 */ public static final @NotNull Icon TIME_PICKER = load("studio/icons/layout-editor/palette/time-picker.svg", -717166004, 2);
      /** 16x16 */ public static final @NotNull Icon TIME_TEXTFIELD = load("studio/icons/layout-editor/palette/time-textfield.svg", -24140297, 2);
      /** 16x16 */ public static final @NotNull Icon TOGGLE_BUTTON = load("studio/icons/layout-editor/palette/toggle-button.svg", 1943320208, 2);
      /** 16x16 */ public static final @NotNull Icon TOOLBAR = load("studio/icons/layout-editor/palette/toolbar.svg", 1347145309, 2);
      /** 16x16 */ public static final @NotNull Icon UNKNOWN_VIEW = load("studio/icons/layout-editor/palette/unknown-view.svg", -1277208063, 2);
      /** 16x16 */ public static final @NotNull Icon VERTICAL_DIVIDER = load("studio/icons/layout-editor/palette/vertical-divider.svg", -1820062832, 2);
      /** 16x16 */ public static final @NotNull Icon VIDEO_VIEW = load("studio/icons/layout-editor/palette/video-view.svg", 1494954868, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_ANIMATOR = load("studio/icons/layout-editor/palette/view-animator.svg", 843645270, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_FLIPPER = load("studio/icons/layout-editor/palette/view-flipper.svg", -1200022041, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_PAGER = load("studio/icons/layout-editor/palette/view-pager.svg", 2127755635, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_STUB = load("studio/icons/layout-editor/palette/view-stub.svg", 610619836, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_SWITCHER = load("studio/icons/layout-editor/palette/view-switcher.svg", -1658398389, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW = load("studio/icons/layout-editor/palette/view.svg", -990829890, 2);
      /** 16x16 */ public static final @NotNull Icon WEB_VIEW = load("studio/icons/layout-editor/palette/web-view.svg", -2069538185, 2);
    }

    public static final class Properties {
      /** 16x16 */ public static final @NotNull Icon ADD_CONNECTION = load("studio/icons/layout-editor/properties/add-connection.svg", 749295562, 2);
      /** 12x12 */ public static final @NotNull Icon DESIGN_PROPERTY_ENABLED = load("studio/icons/layout-editor/properties/design-property-enabled.svg", 522587599, 2);
      /** 12x12 */ public static final @NotNull Icon DESIGN_PROPERTY = load("studio/icons/layout-editor/properties/design-property.svg", -1991173322, 2);
      /** 16x16 */ public static final @NotNull Icon FAVORITES_HOVER = load("studio/icons/layout-editor/properties/favorites-hover.svg", -2134053598, 2);
      /** 16x16 */ public static final @NotNull Icon FAVORITES = load("studio/icons/layout-editor/properties/favorites.svg", 1292789191, 2);
      /** 14x14 */ public static final @NotNull Icon FLAG = load("studio/icons/layout-editor/properties/flag.svg", -1722272121, 2);
      /** 16x16 */ public static final @NotNull Icon GONE_TOOLS_ATTRIBUTE = load("studio/icons/layout-editor/properties/gone-tools-attribute.svg", -2070471294, 2);
      /** 16x16 */ public static final @NotNull Icon GONE = load("studio/icons/layout-editor/properties/gone.svg", -1838787671, 2);
      /** 16x16 */ public static final @NotNull Icon IMAGE_PICKER = load("studio/icons/layout-editor/properties/image-picker.svg", -2134063082, 2);
      /** 16x16 */ public static final @NotNull Icon INVISIBLE_TOOLS_ATTRIBUTE = load("studio/icons/layout-editor/properties/invisible-tools-attribute.svg", 1078414483, 2);
      /** 16x16 */ public static final @NotNull Icon INVISIBLE = load("studio/icons/layout-editor/properties/invisible.svg", -1754722516, 2);
      /** 16x16 */ public static final @NotNull Icon MODIFY_NAV = load("studio/icons/layout-editor/properties/modify-nav.svg", 1658746259, 2);
      /** 16x16 */ public static final @NotNull Icon MODIFY_TEXT = load("studio/icons/layout-editor/properties/modify-text.svg", 1592094984, 2);
      /** 16x16 */ public static final @NotNull Icon MODIFY_THEME = load("studio/icons/layout-editor/properties/modify-theme.svg", -1748063245, 2);
      /** 16x16 */ public static final @NotNull Icon PACKED_HORIZONTAL = load("studio/icons/layout-editor/properties/packed-horizontal.svg", 1138047465, 2);
      /** 16x16 */ public static final @NotNull Icon PACKED_VERTICAL = load("studio/icons/layout-editor/properties/packed-vertical.svg", 719146434, 2);
      /** 16x16 */ public static final @NotNull Icon SPREAD_HORIZONTAL = load("studio/icons/layout-editor/properties/spread-horizontal.svg", -152262853, 2);
      /** 16x16 */ public static final @NotNull Icon SPREAD_INSIDE_HORIZONTAL = load("studio/icons/layout-editor/properties/spread-inside-horizontal.svg", 1160393623, 2);
      /** 16x16 */ public static final @NotNull Icon SPREAD_INSIDE_VERTICAL = load("studio/icons/layout-editor/properties/spread-inside-vertical.svg", 1916684700, 2);
      /** 16x16 */ public static final @NotNull Icon SPREAD_VERTICAL = load("studio/icons/layout-editor/properties/spread-vertical.svg", -798910591, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_ALIGN_CENTER = load("studio/icons/layout-editor/properties/text-align-center.svg", 1315123000, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_ALIGN_LAYOUT_LEFT = load("studio/icons/layout-editor/properties/text-align-layout-left.svg", -697174883, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_ALIGN_LAYOUT_RIGHT = load("studio/icons/layout-editor/properties/text-align-layout-right.svg", -582309744, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_ALIGN_LEFT = load("studio/icons/layout-editor/properties/text-align-left.svg", -1619500256, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_ALIGN_RIGHT = load("studio/icons/layout-editor/properties/text-align-right.svg", 12183787, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_STYLE_BOLD = load("studio/icons/layout-editor/properties/text-style-bold.svg", -1396508093, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_STYLE_ITALIC = load("studio/icons/layout-editor/properties/text-style-italic.svg", 1933770246, 2);
      /** 16x16 */ public static final @NotNull Icon TEXT_STYLE_UPPERCASE = load("studio/icons/layout-editor/properties/text-style-uppercase.svg", 300974850, 2);
      /** 16x16 */ public static final @NotNull Icon TOGGLE_PROPERTIES = load("studio/icons/layout-editor/properties/toggle-properties.svg", -424655262, 2);
      /** 12x12 */ public static final @NotNull Icon TOOLS_ATTRIBUTE = load("studio/icons/layout-editor/properties/tools-attribute.svg", 998045385, 2);
      /** 16x16 */ public static final @NotNull Icon VISIBLE_TOOLS_ATTRIBUTE = load("studio/icons/layout-editor/properties/visible-tools-attribute.svg", 828068352, 2);
      /** 16x16 */ public static final @NotNull Icon VISIBLE = load("studio/icons/layout-editor/properties/visible.svg", 1867543596, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon ACCESSIBILITY = load("studio/icons/layout-editor/toolbar/accessibility.svg", 1650419735, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_COMPONENT = load("studio/icons/layout-editor/toolbar/add-component.svg", 1194087042, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_LOCALE = load("studio/icons/layout-editor/toolbar/add-locale.svg", 1827207967, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_API = load("studio/icons/layout-editor/toolbar/android-api.svg", -677477691, 2);
      /** 16x16 */ public static final @NotNull Icon ARROW_DOWN = load("studio/icons/layout-editor/toolbar/arrow-down.svg", -1323150640, 2);
      /** 16x16 */ public static final @NotNull Icon ARROW_LEFT = load("studio/icons/layout-editor/toolbar/arrow-left.svg", 1211033349, 2);
      /** 16x16 */ public static final @NotNull Icon ARROW_RIGHT = load("studio/icons/layout-editor/toolbar/arrow-right.svg", -1193136769, 2);
      /** 16x16 */ public static final @NotNull Icon ARROW_UP = load("studio/icons/layout-editor/toolbar/arrow-up.svg", 1708357041, 2);
      /** 16x16 */ public static final @NotNull Icon AUTO_CONNECT = load("studio/icons/layout-editor/toolbar/auto-connect.svg", 1579300638, 2);
      /** 16x16 */ public static final @NotNull Icon AUTO_CORRECT_OFF = load("studio/icons/layout-editor/toolbar/auto-correct-off.svg", -144186336, 2);
      /** 16x16 */ public static final @NotNull Icon BARRIER_HORIZONTAL = load("studio/icons/layout-editor/toolbar/barrier-horizontal.svg", 908538459, 2);
      /** 16x16 */ public static final @NotNull Icon BARRIER_VERTICAL = load("studio/icons/layout-editor/toolbar/barrier-vertical.svg", 493959345, 2);
      /** 16x16 */ public static final @NotNull Icon BASELINE_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/baseline-aligned-constraint.svg", 931268277, 2);
      /** 16x16 */ public static final @NotNull Icon BASELINE_ALIGNED_OFF = load("studio/icons/layout-editor/toolbar/baseline-aligned-off.svg", -1053601199, 2);
      /** 16x16 */ public static final @NotNull Icon BASELINE_ALIGNED = load("studio/icons/layout-editor/toolbar/baseline-aligned.svg", 387232498, 2);
      /** 16x16 */ public static final @NotNull Icon BOTTOM_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/bottom-aligned-constraint.svg", -158032290, 2);
      /** 16x16 */ public static final @NotNull Icon BOTTOM_ALIGNED = load("studio/icons/layout-editor/toolbar/bottom-aligned.svg", -1863182781, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_HORIZONTAL_CONSTRAINT = load("studio/icons/layout-editor/toolbar/center-horizontal-constraint.svg", 1271912619, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_HORIZONTAL_PARENT_CONSTRAINT = load("studio/icons/layout-editor/toolbar/center-horizontal-parent-constraint.svg", -1553262503, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_HORIZONTAL_PARENT = load("studio/icons/layout-editor/toolbar/center-horizontal-parent.svg", -860941138, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_HORIZONTAL = load("studio/icons/layout-editor/toolbar/center-horizontal.svg", 150194016, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_VERTICAL_CONSTRAINT = load("studio/icons/layout-editor/toolbar/center-vertical-constraint.svg", -1086234280, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_VERTICAL_PARENT_CONSTRAINT = load("studio/icons/layout-editor/toolbar/center-vertical-parent-constraint.svg", -352051055, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_VERTICAL_PARENT = load("studio/icons/layout-editor/toolbar/center-vertical-parent.svg", -520612373, 2);
      /** 16x16 */ public static final @NotNull Icon CENTER_VERTICAL = load("studio/icons/layout-editor/toolbar/center-vertical.svg", -1313319760, 2);
      /** 16x16 */ public static final @NotNull Icon CLEAR_CONSTRAINTS = load("studio/icons/layout-editor/toolbar/clear-constraints.svg", -1633196418, 2);
      /** 16x16 */ public static final @NotNull Icon CLEAR_WEIGHT = load("studio/icons/layout-editor/toolbar/clear-weight.svg", 2063572994, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_BOTTOM_TO_BOTTOM = load("studio/icons/layout-editor/toolbar/constrain-bottom-to-bottom.svg", -1824691844, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_BOTTOM_TO_TOP = load("studio/icons/layout-editor/toolbar/constrain-bottom-to-top.svg", 1548844162, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_END_TO_END = load("studio/icons/layout-editor/toolbar/constrain-end-to-end.svg", 406388042, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_END_TO_START = load("studio/icons/layout-editor/toolbar/constrain-end-to-start.svg", -220139799, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_START_TO_END = load("studio/icons/layout-editor/toolbar/constrain-start-to-end.svg", 1624446331, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_START_TO_START = load("studio/icons/layout-editor/toolbar/constrain-start-to-start.svg", -1316245499, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TO_BOTTOM = load("studio/icons/layout-editor/toolbar/constrain-to-bottom.svg", 1107853477, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TO_END = load("studio/icons/layout-editor/toolbar/constrain-to-end.svg", -1149937438, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TO_START = load("studio/icons/layout-editor/toolbar/constrain-to-start.svg", -627684123, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TO_TOP = load("studio/icons/layout-editor/toolbar/constrain-to-top.svg", -1871768591, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TOP_TO_BOTTOM = load("studio/icons/layout-editor/toolbar/constrain-top-to-bottom.svg", 1992447022, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAIN_TOP_TO_TOP = load("studio/icons/layout-editor/toolbar/constrain-top-to-top.svg", -304101495, 2);
      /** 16x16 */ public static final @NotNull Icon CONSTRAINT_SET = load("studio/icons/layout-editor/toolbar/constraint-set.svg", 109695104, 2);
      /** 16x16 */ public static final @NotNull Icon CREATE_CONSTRAINTS = load("studio/icons/layout-editor/toolbar/create-constraints.svg", 286702646, 2);
      /** 16x16 */ public static final @NotNull Icon CREATE_HORIZ_CHAIN = load("studio/icons/layout-editor/toolbar/create-horiz-chain.svg", 1388349384, 2);
      /** 16x16 */ public static final @NotNull Icon CREATE_VERT_CHAIN = load("studio/icons/layout-editor/toolbar/create-vert-chain.svg", -1857135879, 2);
      /** 16x16 */ public static final @NotNull Icon CYCLE_CHAIN_PACKED = load("studio/icons/layout-editor/toolbar/cycle-chain-packed.svg", -562263963, 0);
      /** 16x16 */ public static final @NotNull Icon CYCLE_CHAIN_SPREAD_INLINE = load("studio/icons/layout-editor/toolbar/cycle-chain-spread-inline.svg", -561798960, 0);
      /** 16x16 */ public static final @NotNull Icon CYCLE_CHAIN_SPREAD = load("studio/icons/layout-editor/toolbar/cycle-chain-spread.svg", 312051062, 0);
      /** 16x16 */ public static final @NotNull Icon DEVICE_AUTOMOTIVE = load("studio/icons/layout-editor/toolbar/device-automotive.svg", -1935848685, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_PHONE = load("studio/icons/layout-editor/toolbar/device-phone.svg", -1509035073, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_SCREEN = load("studio/icons/layout-editor/toolbar/device-screen.svg", 1281090825, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_TABLET = load("studio/icons/layout-editor/toolbar/device-tablet.svg", 407573854, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_TV = load("studio/icons/layout-editor/toolbar/device-tv.svg", -1782349090, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_WEAR = load("studio/icons/layout-editor/toolbar/device-wear.svg", 1895840226, 2);
      /** 16x16 */ public static final @NotNull Icon DISTRIBUTE_HORIZONTAL_CONSTRAINT = load("studio/icons/layout-editor/toolbar/distribute-horizontal-constraint.svg", 191453763, 2);
      /** 16x16 */ public static final @NotNull Icon DISTRIBUTE_HORIZONTAL = load("studio/icons/layout-editor/toolbar/distribute-horizontal.svg", 18436996, 2);
      /** 16x16 */ public static final @NotNull Icon DISTRIBUTE_VERTICAL_CONSTRAINT = load("studio/icons/layout-editor/toolbar/distribute-vertical-constraint.svg", -1623123100, 2);
      /** 16x16 */ public static final @NotNull Icon DISTRIBUTE_VERTICAL = load("studio/icons/layout-editor/toolbar/distribute-vertical.svg", -1619633362, 2);
      /** 16x16 */ public static final @NotNull Icon DISTRIBUTE_WEIGHT = load("studio/icons/layout-editor/toolbar/distribute-weight.svg", 1836497818, 2);
      /** 16x16 */ public static final @NotNull Icon EMPTY_FLAG = load("studio/icons/layout-editor/toolbar/empty-flag.svg", -1239363410, 2);
      /** 16x16 */ public static final @NotNull Icon EXPAND_HORIZONTAL = load("studio/icons/layout-editor/toolbar/expand-horizontal.svg", 1130631273, 2);
      /** 16x16 */ public static final @NotNull Icon EXPAND_TO_FIT = load("studio/icons/layout-editor/toolbar/expand-to-fit.svg", 598912478, 2);
      /** 16x16 */ public static final @NotNull Icon EXPAND_VERTICAL = load("studio/icons/layout-editor/toolbar/expand-vertical.svg", -2125588131, 2);
      /** 16x16 */ public static final @NotNull Icon GROUP = load("studio/icons/layout-editor/toolbar/group.svg", 238248286, 2);
      /** 16x16 */ public static final @NotNull Icon GUIDELINE_HORIZONTAL = load("studio/icons/layout-editor/toolbar/guideline-horizontal.svg", 215940118, 2);
      /** 16x16 */ public static final @NotNull Icon GUIDELINE_VERTICAL = load("studio/icons/layout-editor/toolbar/guideline-vertical.svg", 1206123004, 2);
      /** 16x16 */ public static final @NotNull Icon HIDE_CONSTRAINTS = load("studio/icons/layout-editor/toolbar/hide-constraints.svg", -321058064, 2);
      /** 16x16 */ public static final @NotNull Icon HORIZONTAL_CENTER_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/horizontal-center-aligned-constraint.svg", -194999583, 2);
      /** 16x16 */ public static final @NotNull Icon HORIZONTAL_CENTER_ALIGNED = load("studio/icons/layout-editor/toolbar/horizontal-center-aligned.svg", -674716034, 2);
      /** 16x16 */ public static final @NotNull Icon INFER_CONSTRAINTS = load("studio/icons/layout-editor/toolbar/infer-constraints.svg", 1582657121, 2);
      /** 16x16 */ public static final @NotNull Icon INSERT_HORIZ_CHAIN = load("studio/icons/layout-editor/toolbar/insert-horiz-chain.svg", -2099222035, 2);
      /** 16x16 */ public static final @NotNull Icon INSERT_VERT_CHAIN = load("studio/icons/layout-editor/toolbar/insert-vert-chain.svg", -696034407, 2);
      /** 16x16 */ public static final @NotNull Icon LANGUAGE = load("studio/icons/layout-editor/toolbar/language.svg", -415089441, 2);
      /** 16x16 */ public static final @NotNull Icon LAYER = load("studio/icons/layout-editor/toolbar/layer.svg", 401815598, 2);
      /** 16x16 */ public static final @NotNull Icon LEFT_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/left-aligned-constraint.svg", -261850226, 2);
      /** 16x16 */ public static final @NotNull Icon LEFT_ALIGNED = load("studio/icons/layout-editor/toolbar/left-aligned.svg", -640131929, 2);
      /** 16x16 */ public static final @NotNull Icon LOCK = load("studio/icons/layout-editor/toolbar/lock.svg", 139439853, 2);
      /** 16x16 */ public static final @NotNull Icon MARGIN = load("studio/icons/layout-editor/toolbar/margin.svg", -645901929, 2);
      /** 16x16 */ public static final @NotNull Icon MATCH_PARENT_HEIGHT = load("studio/icons/layout-editor/toolbar/match-parent-height.svg", -601034018, 2);
      /** 16x16 */ public static final @NotNull Icon MATCH_PARENT_WIDTH = load("studio/icons/layout-editor/toolbar/match-parent-width.svg", -135048646, 2);
      /** 16x16 */ public static final @NotNull Icon MOVE_DOWN_VERT_CHAIN = load("studio/icons/layout-editor/toolbar/move-down-vert-chain.svg", 666556197, 2);
      /** 16x16 */ public static final @NotNull Icon MOVE_LEFT_HORIZ_CHAIN = load("studio/icons/layout-editor/toolbar/move-left-horiz-chain.svg", -214136575, 2);
      /** 16x16 */ public static final @NotNull Icon MOVE_RIGHT_HORIZ_CHAIN = load("studio/icons/layout-editor/toolbar/move-right-horiz-chain.svg", -441132515, 2);
      /** 16x16 */ public static final @NotNull Icon MOVE_UP_VERT_CHAIN = load("studio/icons/layout-editor/toolbar/move-up-vert-chain.svg", -1863045639, 2);
      /** 16x16 */ public static final @NotNull Icon NORMAL_RENDER = load("studio/icons/layout-editor/toolbar/normal-render.svg", -1151817546, 2);
      /** 16x16 */ public static final @NotNull Icon ORIENT_HORIZONTAL = load("studio/icons/layout-editor/toolbar/orient-horizontal.svg", -1494178099, 2);
      /** 16x16 */ public static final @NotNull Icon ORIENT_VERTICAL = load("studio/icons/layout-editor/toolbar/orient-vertical.svg", 1945871732, 2);
      /** 16x16 */ public static final @NotNull Icon PACK_HORIZONTAL = load("studio/icons/layout-editor/toolbar/pack-horizontal.svg", 374922767, 2);
      /** 16x16 */ public static final @NotNull Icon PACK_VERTICAL = load("studio/icons/layout-editor/toolbar/pack-vertical.svg", 1948345865, 2);
      /** 16x16 */ public static final @NotNull Icon PAN_TOOL_SELECTED = load("studio/icons/layout-editor/toolbar/pan-tool-selected.svg", 485087035, 2);
      /** 16x16 */ public static final @NotNull Icon PAN_TOOL = load("studio/icons/layout-editor/toolbar/pan-tool.svg", 200955884, 2);
      /** 16x16 */ public static final @NotNull Icon PERCENT = load("studio/icons/layout-editor/toolbar/percent.svg", 882830640, 2);
      /** 16x16 */ public static final @NotNull Icon QUESTION = load("studio/icons/layout-editor/toolbar/question.svg", -249964328, 2);
      /** 16x16 */ public static final @NotNull Icon REFRESH = load("studio/icons/layout-editor/toolbar/refresh.svg", 1382800739, 2);
      /** 16x16 */ public static final @NotNull Icon REMOVE_FROM_HORIZ_CHAIN = load("studio/icons/layout-editor/toolbar/remove-from-horiz-chain.svg", -234595517, 2);
      /** 16x16 */ public static final @NotNull Icon REMOVE_FROM_VERT_CHAIN = load("studio/icons/layout-editor/toolbar/remove-from-vert-chain.svg", 1588557001, 2);
      /** 16x16 */ public static final @NotNull Icon RIGHT_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/right-aligned-constraint.svg", 1473324657, 2);
      /** 16x16 */ public static final @NotNull Icon RIGHT_ALIGNED = load("studio/icons/layout-editor/toolbar/right-aligned.svg", -1770766247, 2);
      /** 16x16 */ public static final @NotNull Icon ROTATE_BUTTON = load("studio/icons/layout-editor/toolbar/rotate-button.svg", -1202053320, 2);
      /** 16x16 */ public static final @NotNull Icon SEARCH = load("studio/icons/layout-editor/toolbar/search.svg", -1384069383, 2);
      /** 16x16 */ public static final @NotNull Icon SHOW_CONSTRAINTS = load("studio/icons/layout-editor/toolbar/show-constraints.svg", 2062556031, 2);
      /** 16x16 */ public static final @NotNull Icon THEME_BUTTON = load("studio/icons/layout-editor/toolbar/theme-button.svg", 322938016, 2);
      /** 16x16 */ public static final @NotNull Icon TOOLS_ATTRIBUTE_OFF = load("studio/icons/layout-editor/toolbar/tools-attribute-off.svg", -2048592727, 2);
      /** 16x16 */ public static final @NotNull Icon TOOLS_ATTRIBUTE_ON = load("studio/icons/layout-editor/toolbar/tools-attribute-on.svg", -1759201915, 2);
      /** 16x16 */ public static final @NotNull Icon TOP_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/top-aligned-constraint.svg", 1582081784, 2);
      /** 16x16 */ public static final @NotNull Icon TOP_ALIGNED = load("studio/icons/layout-editor/toolbar/top-aligned.svg", 822895689, 2);
      /** 16x16 */ public static final @NotNull Icon UNLOCK = load("studio/icons/layout-editor/toolbar/unlock.svg", 575842166, 2);
      /** 16x16 */ public static final @NotNull Icon VARIANTS = load("studio/icons/layout-editor/toolbar/variants.svg", 1015755403, 2);
      /** 16x16 */ public static final @NotNull Icon VERTICAL_CENTER_ALIGNED_CONSTRAINT = load("studio/icons/layout-editor/toolbar/vertical-center-aligned-constraint.svg", 1973344216, 2);
      /** 16x16 */ public static final @NotNull Icon VERTICAL_CENTER_ALIGNED = load("studio/icons/layout-editor/toolbar/vertical-center-aligned.svg", -1415180425, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_MODE = load("studio/icons/layout-editor/toolbar/view-mode.svg", -969700549, 2);
      /** 16x16 */ public static final @NotNull Icon VIEWPORT_RENDER = load("studio/icons/layout-editor/toolbar/viewport-render.svg", 916889512, 2);
      /** 16x16 */ public static final @NotNull Icon VIRTUAL_DEVICES = load("studio/icons/layout-editor/toolbar/virtual-devices.svg", 1391241064, 2);
      /** 16x16 */ public static final @NotNull Icon WRAP_HEIGHT = load("studio/icons/layout-editor/toolbar/wrap-height.svg", -1101864367, 2);
      /** 16x16 */ public static final @NotNull Icon WRAP_WIDTH = load("studio/icons/layout-editor/toolbar/wrap-width.svg", 1579517601, 2);
    }
  }

  public static final class LayoutInspector {
    public static final class Panel {
      /** 16x16 */ public static final @NotNull Icon BOTTOM_SWAP = load("studio/icons/layout-inspector/panel/bottom-swap.svg", -1378094077, 2);
      /** 16x16 */ public static final @NotNull Icon BOTTOM = load("studio/icons/layout-inspector/panel/bottom.svg", 1335555102, 2);
      /** 16x16 */ public static final @NotNull Icon LEFT_VERTICAL_SWAP = load("studio/icons/layout-inspector/panel/left-vertical-swap.svg", 1358319061, 2);
      /** 16x16 */ public static final @NotNull Icon LEFT_VERTICAL = load("studio/icons/layout-inspector/panel/left-vertical.svg", -2030297606, 2);
      /** 16x16 */ public static final @NotNull Icon RIGHT_VERTICAL_SWAP = load("studio/icons/layout-inspector/panel/right-vertical-swap.svg", -877277962, 2);
      /** 16x16 */ public static final @NotNull Icon RIGHT_VERTICAL = load("studio/icons/layout-inspector/panel/right-vertical.svg", 782544789, 2);
      /** 16x16 */ public static final @NotNull Icon VERTICAL_SPLIT_SWAP = load("studio/icons/layout-inspector/panel/vertical-split-swap.svg", 1344189960, 2);
      /** 16x16 */ public static final @NotNull Icon VERTICAL_SPLIT = load("studio/icons/layout-inspector/panel/vertical-split.svg", 1794811386, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon CLEAR_OVERLAY = load("studio/icons/layout-inspector/toolbar/clear-overlay.svg", -2073878650, 2);
      /** 16x16 */ public static final @NotNull Icon COLOR_PICKER = load("studio/icons/layout-inspector/toolbar/color-picker.svg", -445896210, 2);
      /** 16x16 */ public static final @NotNull Icon DEGREE = load("studio/icons/layout-inspector/toolbar/degree.svg", 1828702807, 2);
      /** 16x16 */ public static final @NotNull Icon LIVE_UPDATES = load("studio/icons/layout-inspector/toolbar/live-updates.svg", -1116815821, 2);
      /** 16x16 */ public static final @NotNull Icon LOAD_OVERLAY = load("studio/icons/layout-inspector/toolbar/load-overlay.svg", 144853410, 2);
      /** 16x16 */ public static final @NotNull Icon MODE_2D = load("studio/icons/layout-inspector/toolbar/mode-2d.svg", 174309155, 2);
      /** 16x16 */ public static final @NotNull Icon MODE_3D = load("studio/icons/layout-inspector/toolbar/mode-3d.svg", -1318923345, 2);
      /** 16x16 */ public static final @NotNull Icon RECOMPOSITION_CHILDREN_COUNT = load("studio/icons/layout-inspector/toolbar/recomposition-children-count.svg", -1911998265, 2);
      /** 16x16 */ public static final @NotNull Icon RECOMPOSITION_COUNT = load("studio/icons/layout-inspector/toolbar/recomposition-count.svg", 1918763136, 2);
      /** 16x16 */ public static final @NotNull Icon RECOMPOSITION_SKIPPED = load("studio/icons/layout-inspector/toolbar/recomposition-skipped.svg", -191898216, 2);
      /** 16x16 */ public static final @NotNull Icon RESET_VIEW = load("studio/icons/layout-inspector/toolbar/reset-view.svg", -2133948346, 2);
      /** 16x16 */ public static final @NotNull Icon SNAPSHOT = load("studio/icons/layout-inspector/toolbar/snapshot.svg", -1930271296, 2);
    }
  }

  public static final class Logcat {
    public static final class Input {
      /** 16x16 */ public static final @NotNull Icon FAVORITE_FILLED_HOVER = load("studio/icons/logcat/input/favorite-filled-hover.svg", 1945730485, 2);
      /** 16x16 */ public static final @NotNull Icon FAVORITE_FILLED = load("studio/icons/logcat/input/favorite-filled.svg", 1718071905, 2);
      /** 16x16 */ public static final @NotNull Icon FAVORITE_OUTLINE_HOVER = load("studio/icons/logcat/input/favorite-outline-hover.svg", 866374387, 2);
      /** 16x16 */ public static final @NotNull Icon FAVORITE_OUTLINE = load("studio/icons/logcat/input/favorite-outline.svg", -1758283381, 2);
      /** 16x16 */ public static final @NotNull Icon FILTER_HISTORY = load("studio/icons/logcat/input/filter-history.svg", -969064002, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon WRAP_TEXT = load("studio/icons/logcat/toolbar/wrap-text.svg", -1725546250, 2);
    }
  }

  public static final class Misc {
    /** 16x16 */ public static final @NotNull Icon BUILD_TYPE = load("studio/icons/misc/build-type.svg", 1247661484, 2);
    /** 16x16 */ public static final @NotNull Icon DEPENDENCY_CONSUMER = load("studio/icons/misc/dependency-consumer.svg", -1828931853, 2);
    /** 16x16 */ public static final @NotNull Icon DEPENDENCY_PROVIDER = load("studio/icons/misc/dependency-provider.svg", -796647547, 2);
    /** 16x16 */ public static final @NotNull Icon GRADLE_VARIABLE = load("studio/icons/misc/gradle-variable.svg", -1902781616, 2);
    /** 16x16 */ public static final @NotNull Icon PRODUCT_FLAVOR_DIMENSION = load("studio/icons/misc/product-flavor-dimension.svg", -1623441243, 2);
    /** 16x16 */ public static final @NotNull Icon PRODUCT_FLAVOR = load("studio/icons/misc/product-flavor.svg", -1030448487, 2);
    /** 16x16 */ public static final @NotNull Icon PROJECT_SYSTEM_VARIANT = load("studio/icons/misc/project-system-variant.svg", 1438353291, 2);
    /** 16x16 */ public static final @NotNull Icon SIGNING_CONFIG = load("studio/icons/misc/signing-config.svg", -1472892816, 2);
    /** 18x18 */ public static final @NotNull Icon TUTORIAL_INDICATOR = load("studio/icons/misc/tutorial-indicator.svg", 1181588586, 2);
  }

  public static final class NavEditor {
    public static final class ExistingDestinations {
      /** 73x94 */ public static final @NotNull Icon ACTIVITY = load("studio/icons/nav-editor/existing-destinations/activity.svg", 1718621283, 2);
      /** 73x94 */ public static final @NotNull Icon DESTINATION = load("studio/icons/nav-editor/existing-destinations/destination.svg", 1145692037, 2);
      /** 53x78 */ public static final @NotNull Icon NESTED = load("studio/icons/nav-editor/existing-destinations/nested.svg", -1311871230, 2);
      /** 73x94 */ public static final @NotNull Icon PLACEHOLDER = load("studio/icons/nav-editor/existing-destinations/placeholder.svg", -2045987547, 2);
    }

    public static final class Properties {
      /** 16x16 */ public static final @NotNull Icon ACTION = load("studio/icons/nav-editor/properties/action.svg", 1812605910, 2);
      /** 16x16 */ public static final @NotNull Icon ARGUMENT = load("studio/icons/nav-editor/properties/argument.svg", 1981942781, 2);
      /** 16x16 */ public static final @NotNull Icon DEEPLINK = load("studio/icons/nav-editor/properties/deeplink.svg", 1918604670, 2);
      /** 16x16 */ public static final @NotNull Icon GLOBAL_ACTION = load("studio/icons/nav-editor/properties/global-action.svg", 1018534606, 2);
      /** 16x16 */ public static final @NotNull Icon POP_ACTION = load("studio/icons/nav-editor/properties/pop-action.svg", -1149246197, 0);
      /** 16x16 */ public static final @NotNull Icon SOURCE = load("studio/icons/nav-editor/properties/source.svg", 856932828, 2);
    }

    public static final class Surface {
      /** 56x56 */ public static final @NotNull Icon DEEPLINK = load("studio/icons/nav-editor/surface/deeplink.svg", 606713647, 2);
      /** 56x56 */ public static final @NotNull Icon POP_ACTION = load("studio/icons/nav-editor/surface/pop-action.svg", -1286499659, 2);
      /** 56x56 */ public static final @NotNull Icon START_DESTINATION = load("studio/icons/nav-editor/surface/start-destination.svg", 1944648075, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon ACTION = load("studio/icons/nav-editor/toolbar/action.svg", 1812605910, 2);
      /** 16x16 */ public static final @NotNull Icon ADD_DESTINATION = load("studio/icons/nav-editor/toolbar/add-destination.svg", 803882693, 2);
      /** 16x16 */ public static final @NotNull Icon ASSIGN_START = load("studio/icons/nav-editor/toolbar/assign-start.svg", -1542295834, 2);
      /** 16x16 */ public static final @NotNull Icon AUTO_ARRANGE = load("studio/icons/nav-editor/toolbar/auto-arrange.svg", 200057839, 2);
      /** 16x16 */ public static final @NotNull Icon DEEPLINK = load("studio/icons/nav-editor/toolbar/deeplink.svg", 1918604670, 2);
      /** 16x16 */ public static final @NotNull Icon NESTED_GRAPH = load("studio/icons/nav-editor/toolbar/nested-graph.svg", -1570166464, 2);
    }

    public static final class Tree {
      /** 16x16 */ public static final @NotNull Icon ACTIVITY = load("studio/icons/nav-editor/tree/activity.svg", -482704453, 2);
      /** 16x16 */ public static final @NotNull Icon FRAGMENT = load("studio/icons/nav-editor/tree/fragment.svg", 781547970, 2);
      /** 16x16 */ public static final @NotNull Icon INCLUDE_GRAPH = load("studio/icons/nav-editor/tree/include-graph.svg", 251198265, 2);
      /** 16x16 */ public static final @NotNull Icon NESTED_GRAPH = load("studio/icons/nav-editor/tree/nested-graph.svg", -1570166464, 2);
      /** 16x16 */ public static final @NotNull Icon PLACEHOLDER = load("studio/icons/nav-editor/tree/placeholder.svg", -194814058, 2);
    }
  }

  public static final class Profiler {
    public static final class Events {
      /** 22x22 */ public static final @NotNull Icon ALLOCATION_TRACKING_FULL = load("studio/icons/profiler/events/allocation-tracking-full.svg", -1108018870, 2);
      /** 22x22 */ public static final @NotNull Icon ALLOCATION_TRACKING_NONE = load("studio/icons/profiler/events/allocation-tracking-none.svg", -421418888, 2);
      /** 22x22 */ public static final @NotNull Icon ALLOCATION_TRACKING_SAMPLED = load("studio/icons/profiler/events/allocation-tracking-sampled.svg", -425947250, 2);
      /** 22x22 */ public static final @NotNull Icon BACK_BUTTON = load("studio/icons/profiler/events/back-button.svg", 572850009, 2);
      /** 22x22 */ public static final @NotNull Icon GARBAGE_EVENT = load("studio/icons/profiler/events/garbage-event.svg", -1753788877, 2);
      /** 22x22 */ public static final @NotNull Icon GPS = load("studio/icons/profiler/events/gps.svg", 1348483479, 2);
      /** 22x22 */ public static final @NotNull Icon KEYBOARD_EVENT = load("studio/icons/profiler/events/keyboard-event.svg", -1940735022, 2);
      /** 22x22 */ public static final @NotNull Icon ROTATE_EVENT = load("studio/icons/profiler/events/rotate-event.svg", 1418475213, 2);
      /** 22x22 */ public static final @NotNull Icon VOLUME_DOWN = load("studio/icons/profiler/events/volume-down.svg", 354442114, 2);
      /** 22x22 */ public static final @NotNull Icon VOLUME_UP = load("studio/icons/profiler/events/volume-up.svg", -277385024, 2);
    }

    public static final class Memoryleakstask {
      /** 16x16 */ public static final @NotNull Icon LEAK_MAYBE = load("studio/icons/profiler/memoryleakstask/leak-maybe.svg", -68675551, 2);
      /** 16x16 */ public static final @NotNull Icon LEAK_NO = load("studio/icons/profiler/memoryleakstask/leak-no.svg", 900859332, 2);
      /** 16x16 */ public static final @NotNull Icon LEAK_YES = load("studio/icons/profiler/memoryleakstask/leak-yes.svg", -560481010, 2);
    }

    public static final class Overlays {
      /** 16x16 */ public static final @NotNull Icon ARRAY_STACK = load("studio/icons/profiler/overlays/array-stack.svg", 518495202, 2);
      /** 16x16 */ public static final @NotNull Icon CLASS_STACK = load("studio/icons/profiler/overlays/class-stack.svg", 241418923, 2);
      /** 16x16 */ public static final @NotNull Icon FIELD_STACK = load("studio/icons/profiler/overlays/field-stack.svg", 741234563, 2);
      /** 16x16 */ public static final @NotNull Icon INTERFACE_STACK = load("studio/icons/profiler/overlays/interface-stack.svg", -1858494276, 2);
      /** 16x16 */ public static final @NotNull Icon METHOD_STACK = load("studio/icons/profiler/overlays/method-stack.svg", -1148961350, 2);
      /** 16x16 */ public static final @NotNull Icon PACKAGE_STACK = load("studio/icons/profiler/overlays/package-stack.svg", -1923638621, 2);
      /** 16x16 */ public static final @NotNull Icon THREAD_SUSPENDED_STACK = load("studio/icons/profiler/overlays/thread-suspended-stack.svg", -797534936, 2);
      /** 20x20 */ public static final @NotNull Icon TRANSPARENT_TILE = load("studio/icons/profiler/overlays/transparent-tile.svg", -1135648570, 2);
    }

    public static final class Sessions {
      /** 16x16 */ public static final @NotNull Icon ALLOCATIONS = load("studio/icons/profiler/sessions/allocations.svg", 1506337220, 2);
      /** 16x16 */ public static final @NotNull Icon BOOKMARK = load("studio/icons/profiler/sessions/bookmark.svg", 1656145071, 2);
      /** 16x16 */ public static final @NotNull Icon CPU = load("studio/icons/profiler/sessions/cpu.svg", 1840363690, 2);
      /** 16x16 */ public static final @NotNull Icon HEAP = load("studio/icons/profiler/sessions/heap.svg", 236876121, 2);
      /** 16x16 */ public static final @NotNull Icon SAVE = load("studio/icons/profiler/sessions/save.svg", -317442787, 2);
    }

    public static final class Sidebar {
      /** 44x44 */ public static final @NotNull Icon FILE = load("studio/icons/profiler/sidebar/file.svg", 509506478, 2);
      /** 44x44 */ public static final @NotNull Icon ISSUE = load("studio/icons/profiler/sidebar/issue.svg", -431570846, 2);
    }

    public static final class Tasks {
      /** 16x16 */ public static final @NotNull Icon APP_STARTUP_INSIGHTS = load("studio/icons/profiler/tasks/app-startup-insights.svg", 603983083, 2);
      /** 16x16 */ public static final @NotNull Icon CALLSTACK_SAMPLE = load("studio/icons/profiler/tasks/callstack-sample.svg", 1772817627, 2);
      /** 16x16 */ public static final @NotNull Icon FIND_MEMORY_LEAKS = load("studio/icons/profiler/tasks/find-memory-leaks.svg", -1670748281, 2);
      /** 16x16 */ public static final @NotNull Icon HEAP_DUMP = load("studio/icons/profiler/tasks/heap-dump.svg", 1704234864, 2);
      /** 16x16 */ public static final @NotNull Icon IMPROVE_ENERGY_USE = load("studio/icons/profiler/tasks/improve-energy-use.svg", 204331505, 2);
      /** 16x16 */ public static final @NotNull Icon JAVA_KOTLIN_ALLOCATIONS = load("studio/icons/profiler/tasks/java-kotlin-allocations.svg", 1344638823, 2);
      /** 16x16 */ public static final @NotNull Icon JAVA_KOTLIN_METHOD_TRACE = load("studio/icons/profiler/tasks/java-kotlin-method-trace.svg", -1154241778, 2);
      /** 16x16 */ public static final @NotNull Icon LIVE_VIEW = load("studio/icons/profiler/tasks/live-view.svg", 344867750, 2);
      /** 16x16 */ public static final @NotNull Icon NATIVE_ALLOCATIONS = load("studio/icons/profiler/tasks/native-allocations.svg", 442428714, 2);
      /** 16x16 */ public static final @NotNull Icon REDUCE_JANK = load("studio/icons/profiler/tasks/reduce-jank.svg", 734074642, 2);
      /** 16x16 */ public static final @NotNull Icon SYSTEM_TRACE = load("studio/icons/profiler/tasks/system-trace.svg", 1100282139, 2);
      /** 16x16 */ public static final @NotNull Icon VIEW_POWER_CONSUMPTION = load("studio/icons/profiler/tasks/view-power-consumption.svg", -1094748326, 2);
    }

    public static final class Taskslarge {
      /** 48x48 */ public static final @NotNull Icon APP_STARTUP_INSIGHTS_LARGE = load("studio/icons/profiler/taskslarge/app-startup-insights-large.svg", -1662215343, 2);
      /** 48x48 */ public static final @NotNull Icon CALLSTACK_SAMPLE_LARGE = load("studio/icons/profiler/taskslarge/callstack-sample-large.svg", -264075059, 2);
      /** 48x48 */ public static final @NotNull Icon FIND_MEMORY_LEAKS_LARGE = load("studio/icons/profiler/taskslarge/find-memory-leaks-large.svg", 572005837, 2);
      /** 48x48 */ public static final @NotNull Icon HEAP_DUMP_LARGE = load("studio/icons/profiler/taskslarge/heap-dump-large.svg", -1149701620, 2);
      /** 48x48 */ public static final @NotNull Icon IMPROVE_ENERGY_USE_LARGE = load("studio/icons/profiler/taskslarge/improve-energy-use-large.svg", 1235765822, 2);
      /** 48x48 */ public static final @NotNull Icon JAVA_KOTLIN_ALLOCATIONS_LARGE = load("studio/icons/profiler/taskslarge/java-kotlin-allocations-large.svg", -552148026, 2);
      /** 48x48 */ public static final @NotNull Icon JAVA_KOTLIN_METHOD_TRACE_LARGE = load("studio/icons/profiler/taskslarge/java-kotlin-method-trace-large.svg", -1502874232, 2);
      /** 48x48 */ public static final @NotNull Icon LIVE_VIEW_LARGE = load("studio/icons/profiler/taskslarge/live-view-large.svg", -116688258, 2);
      /** 48x48 */ public static final @NotNull Icon NATIVE_ALLOCATIONS_LARGE = load("studio/icons/profiler/taskslarge/native-allocations-large.svg", 2093349744, 2);
      /** 48x48 */ public static final @NotNull Icon REDUCE_JANK_LARGE = load("studio/icons/profiler/taskslarge/reduce-jank-large.svg", -555001009, 2);
      /** 48x48 */ public static final @NotNull Icon SYSTEM_TRACE_LARGE = load("studio/icons/profiler/taskslarge/system-trace-large.svg", 823950869, 2);
      /** 48x48 */ public static final @NotNull Icon VIEW_POWER_CONSUMPTION_LARGE = load("studio/icons/profiler/taskslarge/view-power-consumption-large.svg", -1262709061, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon CAPTURE_CLOCK = load("studio/icons/profiler/toolbar/capture-clock.svg", 2147007519, 0);
      /** 16x16 */ public static final @NotNull Icon CLOCK = load("studio/icons/profiler/toolbar/clock.svg", -1534618337, 2);
      /** 16x16 */ public static final @NotNull Icon COLLAPSE_SESSION = load("studio/icons/profiler/toolbar/collapse-session.svg", 683856057, 2);
      /** 16x16 */ public static final @NotNull Icon EXPAND_SESSION = load("studio/icons/profiler/toolbar/expand-session.svg", -1640831398, 2);
      /** 16x16 */ public static final @NotNull Icon FORCE_GARBAGE_COLLECTION = load("studio/icons/profiler/toolbar/force-garbage-collection.svg", 1588699173, 2);
      /** 16x16 */ public static final @NotNull Icon GOTO_LIVE = load("studio/icons/profiler/toolbar/goto-live.svg", 968367996, 2);
      /** 16x16 */ public static final @NotNull Icon HEAP_DUMP = load("studio/icons/profiler/toolbar/heap-dump.svg", 100208999, 2);
      /** 16x16 */ public static final @NotNull Icon PAUSE_LIVE = load("studio/icons/profiler/toolbar/pause-live.svg", -1035564376, 2);
      /** 16x16 */ public static final @NotNull Icon RECORD = load("studio/icons/profiler/toolbar/record.svg", 1545996896, 2);
      /** 16x16 */ public static final @NotNull Icon SELECT_ENTIRE_RANGE = load("studio/icons/profiler/toolbar/select-entire-range.svg", -1499276971, 2);
      /** 16x16 */ public static final @NotNull Icon STOP_RECORDING = load("studio/icons/profiler/toolbar/stop-recording.svg", -363825005, 2);
      /** 16x16 */ public static final @NotNull Icon STOP_SESSION = load("studio/icons/profiler/toolbar/stop-session.svg", 1809627912, 2);
      /** 16x16 */ public static final @NotNull Icon UPLOAD = load("studio/icons/profiler/toolbar/upload.svg", -255229088, 2);
    }
  }

  public static final class Shell {
    public static final class Filetree {
      /** 16x16 */ public static final @NotNull Icon ACTIVITY = load("studio/icons/shell/filetree/activity.svg", 229793721, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_FILE = load("studio/icons/shell/filetree/android-file.svg", 1597748289, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_MODULE = load("studio/icons/shell/filetree/android-module.svg", -693565144, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_PROJECT = load("studio/icons/shell/filetree/android-project.svg", -1440924135, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_TEST_ROOT = load("studio/icons/shell/filetree/android-test-root.svg", 705488813, 2);
      /** 16x16 */ public static final @NotNull Icon CONFIG_FILE = load("studio/icons/shell/filetree/config-file.svg", -47316812, 2);
      /** 16x16 */ public static final @NotNull Icon DATABASE_FILE = load("studio/icons/shell/filetree/database-file.svg", -375152459, 2);
      /** 16x16 */ public static final @NotNull Icon FEATURE_MODULE = load("studio/icons/shell/filetree/feature-module.svg", -1240334459, 2);
      /** 16x16 */ public static final @NotNull Icon FONT_FILE = load("studio/icons/shell/filetree/font-file.svg", -917319335, 2);
      /** 16x16 */ public static final @NotNull Icon GRADLE_FILE = load("studio/icons/shell/filetree/gradle-file.svg", -1521563568, 2);
      /** 16x16 */ public static final @NotNull Icon INSTANT_APPS = load("studio/icons/shell/filetree/instant-apps.svg", 783764162, 2);
      /** 16x16 */ public static final @NotNull Icon LIBRARY_MODULE = load("studio/icons/shell/filetree/library-module.svg", 335188456, 2);
      /** 16x16 */ public static final @NotNull Icon LIBRARY_UNKNOWN = load("studio/icons/shell/filetree/library-unknown.svg", -857185708, 2);
      /** 16x16 */ public static final @NotNull Icon LIBRARY_WARNING = load("studio/icons/shell/filetree/library-warning.svg", 1200645021, 2);
      /** 16x16 */ public static final @NotNull Icon MANIFEST_FILE = load("studio/icons/shell/filetree/manifest-file.svg", -1679005845, 2);
      /** 16x16 */ public static final @NotNull Icon MAVEN = load("studio/icons/shell/filetree/maven.svg", 939865208, 2);
      /** 16x16 */ public static final @NotNull Icon RENDER_SCRIPT = load("studio/icons/shell/filetree/render-script.svg", -166583629, 2);
      /** 16x16 */ public static final @NotNull Icon SCENEFORM_ASSET_DESCRIPTION_FILE = load("studio/icons/shell/filetree/sceneform-asset-description-file.svg", 597857109, 2);
      /** 16x16 */ public static final @NotNull Icon SCENEFORM_BINARY = load("studio/icons/shell/filetree/sceneform-binary.svg", 662361462, 2);
      /** 16x16 */ public static final @NotNull Icon TFLITE_FILE = load("studio/icons/shell/filetree/tflite-file.svg", 2025103701, 2);
    }

    public static final class Menu {
      /** 16x16 */ public static final @NotNull Icon AVD_MANAGER = load("studio/icons/shell/menu/avd-manager.svg", 1855951796, 2);
      /** 16x16 */ public static final @NotNull Icon DATABASE_INSPECTOR = load("studio/icons/shell/menu/database-inspector.svg", 36758956, 2);
      /** 16x16 */ public static final @NotNull Icon LAYOUT_INSPECTOR = load("studio/icons/shell/menu/layout-inspector.svg", -2115952362, 2);
      /** 16x16 */ public static final @NotNull Icon NETWORK_INSPECTOR = load("studio/icons/shell/menu/network-inspector.svg", -165531104, 2);
      /** 16x16 */ public static final @NotNull Icon PROFILER = load("studio/icons/shell/menu/profiler.svg", -1122780795, 2);
      /** 16x16 */ public static final @NotNull Icon STUDIO_LABS = load("studio/icons/shell/menu/studio-labs.svg", -1489449878, 2);
      /** 16x16 */ public static final @NotNull Icon THEME_EDITOR = load("studio/icons/shell/menu/theme-editor.svg", -1935619734, 2);
    }

    public static final class StatusBar {
      /** 16x16 */ public static final @NotNull Icon ADB_MANAGED = load("studio/icons/shell/status-bar/adb-managed.svg", -1720914442, 2);
      /** 16x16 */ public static final @NotNull Icon ADB_UNMANAGED = load("studio/icons/shell/status-bar/adb-unmanaged.svg", -615894156, 2);
      /** 16x16 */ public static final @NotNull Icon ESSENTIALS_MODE = load("studio/icons/shell/status-bar/essentials-mode.svg", 219599927, 2);
    }

    public static final class Telemetry {
      /** 16x16 */ public static final @NotNull Icon SEND_FEEDBACK = load("studio/icons/shell/telemetry/send-feedback.svg", -1057598064, 2);
    }

    public static final class ToolWindows {
      /** 16x16 */ public static final @NotNull Icon AGP_UPGRADE_ASSISTANT = load("studio/icons/shell/tool-windows/agp-upgrade-assistant.svg", 1302187963, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_PROFILER = load("studio/icons/shell/tool-windows/android-profiler.svg", -1738722722, 2);
      /** 16x16 */ public static final @NotNull Icon APP_LINKS_ASSISTANT = load("studio/icons/shell/tool-windows/app-links-assistant.svg", 400858850, 2);
      /** 16x16 */ public static final @NotNull Icon APP_QUALITY_INSIGHTS = load("studio/icons/shell/tool-windows/app-quality-insights.svg", -2006768636, 2);
      /** 16x16 */ public static final @NotNull Icon ASSISTANT = load("studio/icons/shell/tool-windows/assistant.svg", 1007557999, 2);
      /** 16x16 */ public static final @NotNull Icon ATTRIBUTES = load("studio/icons/shell/tool-windows/attributes.svg", -1524733236, 2);
      /** 16x16 */ public static final @NotNull Icon BUILD_VARIANTS = load("studio/icons/shell/tool-windows/build-variants.svg", 47953949, 2);
      /** 16x16 */ public static final @NotNull Icon CAPTURES = load("studio/icons/shell/tool-windows/captures.svg", 1768509579, 2);
      /** 16x16 */ public static final @NotNull Icon COMPONENT_TREE = load("studio/icons/shell/tool-windows/component-tree.svg", -1700470753, 2);
      /** 16x16 */ public static final @NotNull Icon DATABASE_INSPECTOR = load("studio/icons/shell/tool-windows/database-inspector.svg", 36758956, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_EXPLORER = load("studio/icons/shell/tool-windows/device-explorer.svg", 504905529, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_MANAGER = load("studio/icons/shell/tool-windows/device-manager.svg", -391569065, 2);
      /** 16x16 */ public static final @NotNull Icon EMULATOR = load("studio/icons/shell/tool-windows/emulator.svg", 1546052772, 2);
      /** 16x16 */ public static final @NotNull Icon INSPECTION = load("studio/icons/shell/tool-windows/inspection.svg", -1518200243, 2);
      /** 16x16 */ public static final @NotNull Icon LINT = load("studio/icons/shell/tool-windows/lint.svg", 157085806, 2);
      /** 16x16 */ public static final @NotNull Icon LOGCAT = load("studio/icons/shell/tool-windows/logcat.svg", -2104326536, 2);
      /** 16x16 */ public static final @NotNull Icon MULTI_PREVIEW = load("studio/icons/shell/tool-windows/multi-preview.svg", -827767205, 2);
      /** 16x16 */ public static final @NotNull Icon PALETTE = load("studio/icons/shell/tool-windows/palette.svg", -1263713902, 2);
      /** 16x16 */ public static final @NotNull Icon STRUCTURE = load("studio/icons/shell/tool-windows/structure.svg", 26239246, 2);
      /** 16x16 */ public static final @NotNull Icon VISUAL_ASSETS = load("studio/icons/shell/tool-windows/visual-assets.svg", -999735342, 2);
    }

    public static final class Toolbar {
      /** 16x16 */ public static final @NotNull Icon APPLY_ALL_CHANGES = load("studio/icons/shell/toolbar/apply-all-changes.svg", -427969367, 2);
      /** 16x16 */ public static final @NotNull Icon APPLY_CODE_SWAP = load("studio/icons/shell/toolbar/apply-code-swap.svg", 862965334, 2);
      /** 16x16 */ public static final @NotNull Icon ATTACH_DEBUGGER = load("studio/icons/shell/toolbar/attach-debugger.svg", 1651912358, 2);
      /** 16x16 */ public static final @NotNull Icon BUILD_MODULE = load("studio/icons/shell/toolbar/build-module.svg", -1626902606, 2);
      /** 16x16 */ public static final @NotNull Icon BUILD_RUN_CONFIGURATION = load("studio/icons/shell/toolbar/build-run-configuration.svg", -1690483132, 2);
      /** 16x16 */ public static final @NotNull Icon DEVICE_MANAGER = load("studio/icons/shell/toolbar/device-manager.svg", 393647671, 2);
      /** 16x16 */ public static final @NotNull Icon GRADLE_SYNC = load("studio/icons/shell/toolbar/gradle-sync.svg", -290130938, 2);
      /** 16x16 */ public static final @NotNull Icon PROFILER_DETAILED = load("studio/icons/shell/toolbar/profiler-detailed.svg", -170456015, 2);
      /** 16x16 */ public static final @NotNull Icon PROFILER_LOW_OVERHEAD = load("studio/icons/shell/toolbar/profiler-low-overhead.svg", 1605566745, 2);
      /** 16x16 */ public static final @NotNull Icon PROFILER = load("studio/icons/shell/toolbar/profiler.svg", -984809095, 2);
      /** 16x16 */ public static final @NotNull Icon SDK_MANAGER = load("studio/icons/shell/toolbar/sdk-manager.svg", 1043393530, 2);
      /** 16x16 */ public static final @NotNull Icon USER_PROFILE_ACTIVE = load("studio/icons/shell/toolbar/user-profile-active.svg", 258211818, 2);
      /** 16x16 */ public static final @NotNull Icon USER_PROFILE = load("studio/icons/shell/toolbar/user-profile.svg", -1695942103, 2);
    }
  }

  public static final class StudioBot {
    /** 16x16 */ public static final @NotNull Icon ASK = load("studio/icons/studio-bot/ask.svg", -2146507028, 2);
    /** 16x16 */ public static final @NotNull Icon LOGO_MONOCHROME = load("studio/icons/studio-bot/logo-monochrome.svg", -1548631275, 2);
    /** 16x16 */ public static final @NotNull Icon LOGO = load("studio/icons/studio-bot/logo.svg", 1473884075, 2);
  }

  public static final class Test {
    /** 16x16 */ public static final @NotNull Icon RECORD_ESPRESSO_TEST = load("studio/icons/test/record-espresso-test.svg", 1394641221, 2);
    /** 16x16 */ public static final @NotNull Icon RICH_TEST_RESULT_ERROR = load("studio/icons/test/rich-test-result-error.svg", -1311703449, 2);
    /** 16x16 */ public static final @NotNull Icon RICH_TEST_RESULT_FAILED = load("studio/icons/test/rich-test-result-failed.svg", 1958309766, 2);
    /** 16x16 */ public static final @NotNull Icon RICH_TEST_RESULT_PASSED = load("studio/icons/test/rich-test-result-passed.svg", 148874082, 2);
  }

  public static final class Wear {
    /** 16x16 */ public static final @NotNull Icon COMPLICATIONS_RUN_CONFIG = load("studio/icons/wear/complications-run-config.svg", 1089669639, 2);
    /** 16x16 */ public static final @NotNull Icon TILES_RUN_CONFIG = load("studio/icons/wear/tiles-run-config.svg", 218102322, 2);
    /** 16x16 */ public static final @NotNull Icon WATCH_FACE_RUN_CONFIG = load("studio/icons/wear/watch-face-run-config.svg", -1436096234, 2);
  }

  public static final class Wizards {
    public static final class Modules {
      /** 16x16 */ public static final @NotNull Icon ANDROID_LIBRARY = load("studio/icons/wizards/modules/android-library.svg", 1438440829, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_THINGS = load("studio/icons/wizards/modules/android-things.svg", 1722610405, 2);
      /** 16x16 */ public static final @NotNull Icon ANDROID_TV = load("studio/icons/wizards/modules/android-tv.svg", -1054231248, 2);
      /** 16x16 */ public static final @NotNull Icon AUTOMOTIVE = load("studio/icons/wizards/modules/automotive.svg", -1566043471, 2);
      /** 15x14 */ public static final @NotNull Icon BASELINE_PROFILE_GENERATE = load("studio/icons/wizards/modules/baseline-profile-generate.svg", 304045514, 2);
      /** 16x16 */ public static final @NotNull Icon BASELINE_PROFILE = load("studio/icons/wizards/modules/baseline-profile.svg", 2079232880, 2);
      /** 16x16 */ public static final @NotNull Icon BENCHMARK = load("studio/icons/wizards/modules/benchmark.svg", 1124086582, 2);
      /** 16x16 */ public static final @NotNull Icon DYNAMIC_FEATURE = load("studio/icons/wizards/modules/dynamic-feature.svg", -167197343, 2);
      /** 16x16 */ public static final @NotNull Icon INSTANT_DYNAMIC_FEATURE = load("studio/icons/wizards/modules/instant-dynamic-feature.svg", -989882169, 2);
      /** 16x16 */ public static final @NotNull Icon NATIVE = load("studio/icons/wizards/modules/native.svg", -1377682745, 2);
      /** 16x16 */ public static final @NotNull Icon PHONE_TABLET = load("studio/icons/wizards/modules/phone-tablet.svg", 971433777, 2);
      /** 16x16 */ public static final @NotNull Icon WEAR_OS = load("studio/icons/wizards/modules/wear-os.svg", 2067825988, 2);
    }
  }
}
